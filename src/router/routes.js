
const routes = [
	{
		path: '/',
		component: () => import('layouts/Layout.vue'),
		children: [
			{ path: '', component: () => import('pages/Index.vue') },
			{ path: 'about', component: () => import('pages/About.vue') },
			{ path: 'apps', component: () => import('pages/Apps.vue') },
			{ path: 'assistant', component: () => import('pages/Assistant.vue') },
			{ path: 'assistant/:step', component: () => import('pages/Assistant.vue') },
			{ path: 'embed', component: () => import('pages/Embed.vue') },
			{ path: 'information', component: () => import('pages/Information.vue') },
			{ path: 'logs', component: () => import('pages/Logs.vue') },
			{ path: 'logs/:unit', component: () => import('pages/Logs.vue') },
			{ path: 'map', component: () => import('pages/Map.vue') },
			{ path: 'mediathekhbbtv', component: () => import('pages/MediathekHbbTV.vue') },
			{ path: 'mediathekview', component: () => import('pages/MediathekView.vue') },
			{ path: 'packages', component: () => import('pages/Packages.vue') },
			{ path: 'packages/:tab', component: () => import('pages/Packages.vue') },
			{ path: 'services', component: () => import('pages/Services.vue') },
			{ path: 'settings', component: () => import('pages/Settings.vue') },
			{ path: 'support', component: () => import('pages/Support.vue') },
			{ path: 'vdrmpv', component: () => import('pages/VdrMpv.vue') },
			{ path: 'vdrosd', component: () => import('pages/VdrOsd.vue') },
			{ path: 'vdrstreamdev', component: () => import('pages/VdrStreamdev.vue') },
		]
	},
	{
		path: '*',
		component: () => import('layouts/Layout.vue'),
		children: [
			{ path: '*', component: () => import('pages/Error404.vue') },
		]
	}
]

export default routes
