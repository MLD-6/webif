import de from './de.js'
import en from './en.js'

export default {
    de: de,
    en: en,    
}
