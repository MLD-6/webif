import Vue from 'vue'
import io from 'socket.io-client'

// const url = window.location.port == 8080 ? 'http://192.168.178.111' : ''
// const url = window.location.port == 8080 ? 'http://192.168.178.5' : ''
// const url = window.location.port == 8080 ? 'http://192.168.178.105' : ''
// const url = window.location.port == 8080 ? 'http://192.168.178.13' : ''
const url = window.location.port == 8080 ? 'http://'+window.location.hostname : ''

const proxy = {
	get: (target, key) => {
		let property = Object.getOwnPropertyDescriptor(target, key)
		let type = Object.prototype.toString.call(property?.value)
		if ((type == '[object Object]' || type == '[object Array]' || property?.get && property?.enumerable && !updater.noCache) && !target[key].__ob__) {
			Object.defineProperty(target[key], '__namespace__', {enumerable: false, writable: true, value: [...target.__namespace__ || [], key]})
			return new Proxy(target[key], proxy)
		}
		return target[key]
	},
	set: (target, key, value) => {
		socket.emit('set', [...target.__namespace__ || [], key], value)
		if (Object.getOwnPropertyDescriptor(target, key)?.set) {
			updater.version++
		} else {
			// target[key] = JSON.parse(JSON.stringify(value))
		}
		return true
	},
	deleteProperty: (target, key) => {
		socket.emit('del', [...target.__namespace__ || [], key])
		if (Object.getOwnPropertyDescriptor(target, key)?.set) {
			updater.version++
		} else if (Array.isArray(target)) {
			target.splice(key, 1)
		} else {
			// delete target[key]
		}
		return true
	}
}

function mergeHandlers(store, handlers) {
	for (let key in handlers) {
		let property = Object.getOwnPropertyDescriptor(store, key)
		if (typeof handlers[key] == 'object') {
			if (typeof property?.value != 'object') {
				store[key] = {}
			}
			mergeHandlers(store[key], handlers[key])
		} else if (handlers[key] == 'function') {
			store[key] = (...args) => {
				return new Promise(resolve => {
					socket.emit('call', [...store.__namespace__, key], args, resolve)
				})
			}
		} else {
			if ((!!handlers[key].match('getter')) != (typeof property?.get == 'function')) {
				let version = 0
				let value = undefined
				Object.defineProperty(store, key, {
					get: !handlers[key].match('getter') ? undefined : () => {
						if (version != updater.version || updater.noCache) {
							version = updater.version
							let newvalue = new Promise(resolve => {
								socket.emit('get', [...store.__namespace__, key], result => {
									if (JSON.stringify(value) != JSON.stringify(result)) {
										value = result
										clearTimeout(updater.refrashTimer)
										updater.refrashTimer = setTimeout(()=>{updater.refresh++}, 100)
										if (result && typeof result == 'object') {
											Object.defineProperty(store, key, {enumerable: true})
											if (handlers[key].match('setter')) {
												Object.defineProperty(result, '__namespace__', {enumerable: false, writable: true, value: [...store.__namespace__ || [], key]})
												return resolve(new Proxy(result, proxy))
											}
										}
									}
									// clear cache
									setTimeout(() => {version = 0}, 5000)
									resolve(result)
								})
							})
							if (value === undefined || updater.noCache) value = newvalue
							updater.noCache = false
						}
						return value
					},
					configurable: true
				})
			}
			if ((!!handlers[key].match('setter')) != (typeof property?.set == 'function')) {
				Object.defineProperty(store, key, {
					set: !handlers[key].match('setter') ? undefined : () => {},
					configurable: true
				})
			}
		}
	}
}

function mergeSettings(store, settings) {
	for (let key in settings) {
		let property = Object.getOwnPropertyDescriptor(store, key)
		if (settings[key] && typeof settings[key] == 'object' && typeof property?.value == 'object') {
			mergeSettings(store[key], settings[key])
		} else if (typeof property?.get != 'function' && typeof property?.value != 'function') {
			store[key] = settings[key]
		}
	}
	Object.keys(store).forEach(key => {
		let property = Object.getOwnPropertyDescriptor(store, key)
		if (settings[key]===undefined && typeof property?.get != 'function' && typeof property?.value != 'function' && typeof property?.value != 'object') {
			delete store[key]
		}
	})
}

function update(settings, handlers) {
	mergeHandlers(store, handlers)
	mergeSettings(store, settings)
	while (listeners.length) {listeners.shift()()}
	updater.version++
}


const listeners = []
const store = {}
const settings = new Proxy(store, proxy)
const socket = io(url + '/settings')
var updater = Vue.observable({version: 0, refresh: 0})

socket.on('update', update)

Vue.mixin({
	computed: {
		'$settings'() {
			updater.version // reactive update
			updater.refresh // reactive update
			return Object.keys(settings).length ? settings : new Promise(resolve => {listeners.push(() => {resolve(settings)})})
		},
		'$socket'() {
			 return socket
		}
	},
	methods: {
		noPromise(obj) {
			return obj && obj.then ? null : obj
		},
		noCache() {
			updater.noCache = true
			return this
		}
	}
})
