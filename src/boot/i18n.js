import Vue from 'vue'
import VueI18n from 'vue-i18n'
import messages from 'src/i18n'

Vue.use(VueI18n)

const i18n = new VueI18n({
	locale: 'en',
	fallbackLocale: {
		'de_DE': ['de'],
		'default': ['en']
	},
	silentFallbackWarn: true,
	messages: {},
	missing: (lc, message, vue) => {
		vue.missing = vue.missing || {}
		if (!vue.missing[message]) {
			vue.missing[message] = message
			if (vue.missing_timout) {
				clearTimeout(vue.missing_timout)
			}
			vue.missing_timout = setTimeout(() => {
				console.log('missing translations:', JSON.stringify(vue.missing, null, '\t').replace(/"/g, "'"))
			}, 1)
		}
	}
})

export default ({ app }) => {
	// Set i18n instance on app
	app.i18n = i18n
}

Vue.mixin({
	created: function () {
		// add component locales to global messages
		if (this.$options.name && this.$options.i18n) {
			for (let lc in messages) {
				let message = messages[lc]
				let path = this.$options.name.split('.')
				let name = path.pop()
				path.forEach(name => {message = message[name]})
				if (message) {
					this.$i18n.mergeLocaleMessage(lc, message[name])
				}
			}
		}
		// use parent translations if none defined for this component
		if (!this.$options.i18n) {
			this.$t = (...args) => this.$tp().$t(...args)
		}
	},

	methods: {
		$tp() {
			let vue = this
			while (!vue.$options.i18n && vue.$parent) {
				vue = vue.$parent
			}
			return vue
		}
	},

	components: {
		// create <t></t> translation tag
		t: {
			render: function() {
				let lcs = [...this.$i18n.fallbackLocale[this.$i18n.locale] || this.$i18n.fallbackLocale['default'], this.$i18n.locale]
				let text = this.$slots.default[0].text
				let te = !this.ite || lcs.find(lc => this.$tp().$te(text, lc))
				let vue = this
				while (!this.ite && vue.$parent && (!vue.$options.i18n || !lcs.find(lc => vue.$te(text, lc)))) {
					vue = vue.$parent
				}
				return this._v(te && vue.$t ? vue.$t(text, this.values) : '')
			},
			props: {
        values: {}, // translation interpolations
				ite: Boolean // if-translation-exists
			}
		},
	}
})
