const express = require('express')
const app = express()
const systemdSocket = require('systemd-socket')
const server = require('http').Server(app)
const io = require('socket.io')(server)
const connection = require('./modules/connection')
const request = require('./modules/request')
const jsonrpc = require('./modules/jsonrpc')

// app.use((req, res, next) => { 
//   console.log(req.method, req.url, req.ip, req.headers) 
// 	next()
// })

io.of('/settings').on('connection', connection)

app.use('/jsonrpc', jsonrpc)
app.use('/setting', request)
app.use(express.static(__dirname + '/www'))
app.get('*', (req, res) => {res.sendFile(__dirname + '/www/')})

const port = process.env.PORT || 80
server.listen(systemdSocket() || port, () => {
	console.log(`Listen on port ${port} ...`)
})
