# MLD Webif (webif)

The MLD webif

## Install the dependencies
```bash
sudo apt install nodejs make g++ libpam0g-dev
npm install -g @quasar/cli nodemon
npm install
```

## Start the app in development mode (hot-code reloading, error reporting, etc.)
The location of the app will be http://localhost:8080/
```bash
quasar dev
nodemon -w connections -w modules -w app.js app.js
```

## Build and start the app for production
The location of the app will be http://localhost:80/
```bash
quasar build
node app.js
```
For wlan configuartion will the tool 'nmcli' be needed.

## Install app
```bash
sudo apt install nodejs make g++ libpam0g-dev
npm install
node_modules/.bin/quasar build
mkdir -p /usr/share/webif
cp webif.menu /usr/share/menu/webif
cp webif.service /lib/systemd/system/
cp -r bin connections modules www app.js package.json /usr/share/webif/
(cd /usr/share/webif; npm install --production; rm package.json)
for file in /usr/share/webif/bin/*; do ln -s $file /usr/bin/; done
```

## Start development Docker container
The location of the app will be http://localhost:8080/
```bash
docker-compose up
```

## Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
