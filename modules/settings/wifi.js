const wifi = require('node-wifi')

module.exports = {
	async networks() {
		wifi.init({iface: null})
		return await wifi.scan().catch(error => {if (error.code != 'ENOENT') console.error(error)})
	},

	async connections() {
		wifi.init({iface: null})
		return await wifi.getCurrentConnections().catch(error => {if (error.code != 'ENOENT' && error.code != 1) console.error(error)})
	},

	async connect(ssid, password) {
		if (this.$root.auth.check() && ssid) {
			wifi.init({iface: null})
			return await wifi.connect({ssid, password}).catch(console.error)
		}
		return false
	},

	async disconnect(iface) {
		if (this.$root.auth.check() && iface) {
			wifi.init({iface})
			return await wifi.disconnect().catch(console.error)
		}
		return false
	},

	async remove(ssid) {
		if (this.$root.auth.check() && ssid) {
			wifi.init({iface: null})
			return await wifi.deleteConnection({ssid}).catch(console.error)
		}
		return false
	},
}
