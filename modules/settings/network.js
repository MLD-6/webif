const os = require('os')
const fs = require('fs')
const fsp = fs.promises
const ini = new (require('conf-cfg-ini'))({lineEnding: '\n', commentIdentifiers: ['#']})
const wol = require('wake_on_lan')

module.exports = {
	get interfaces() {
		return Object.fromEntries(Object.entries(os.networkInterfaces()).filter(([name]) => name.startsWith('eth') || name.startsWith('wlan')))
	},

	async getConfigs() {
		let configs = {}
		for (let interface in this.interfaces) {
			configs[interface] = Object.fromEntries(Object.entries(ini.decode(await fsp.readFile(`/etc/systemd/network/50-${interface}.network`, 'utf8').catch(() => ''))?.Network || {}).map(([k, v]) => [k.toLowerCase(), v]))
			configs[interface].dhcp = configs[interface].dhcp != 'no'
		}
		return configs
	},

	async setConfig(interface, values) {
		if (this.$root.auth.check()) {
			let hasStatic = values.address && values.gateway && values.dns
			let config = {
				Match: {
					Name: interface
				},
				Network: {
					DHCP: values.dhcp !== false || !hasStatic ? 'yes' : 'no'
				}				
			}
			if (hasStatic) {
				Object.assign(config.Network, {
					Address: values.address,
					Gateway: values.gateway,
					DNS: values.dns
				})
			}
			await fsp.writeFile(`/etc/systemd/network/50-${interface}.network`, ini.encode(config))
			return await this.$root.services?.restart('systemd-networkd')
		}
	},

	async removeConfig(interface) {
		if (this.$root.auth.check()) {
			await fsp.rm(`/etc/systemd/network/50-${interface}.network`).catch(() => {})
			return await this.$root.services?.restart('systemd-networkd')
		}
	},

	get wol() {
		return (async () => {return !!((await this.$root.packages.installed()).find(pkg => pkg.name == 'wol'))})()
	},

	set wol(aktive) {
		(async () => {
			if (this.$root.auth.check()) {
				if (aktive) {
					return await this.$root.packages.install('wol')
				} else {
					return await this.$root.packages.remove('wol')
				}
			}
		})()
	},

	wake(mac) {
		wol.wake(mac)
	},

	get ntp() {
		return (async () => {return ini.decode(await fsp.readFile('/etc/systemd/timesyncd.conf.d/local.conf', 'utf8').catch(() => ''))?.Time?.NTP})()
	},
	set ntp(names) {
		(async () => {
			if (this.$root.auth.check()) {
				if (!names) {
					await fsp.rm('/etc/systemd/timesyncd.conf.d/local.conf').catch(() => {})
				} else {
					let config = {
						Time: {
							NTP: names
						}
					}
					await fsp.mkdir('/etc/systemd/timesyncd.conf.d', {recursive: true})
					await fsp.writeFile('/etc/systemd/timesyncd.conf.d/local.conf', ini.encode(config))
				}
				this.$root.services.restart('systemd-timesyncd')
			}
		})()
	}
}
