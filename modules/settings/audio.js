const exec = require('../exec')
const setter = require('../object').setter

module.exports = {
	get active() {
		return setter(this.$root, this._active, () => {updateActive(this._active)})
	},
	set active(value) {
		if (this.$root.auth.check()) {
			this._active = value
			updateActive(this._active)
		}
	},

	get present() {
		return (async () => {
			var card = '', device = 0, presents = [];
			(await exec('alsactl store -f -')).stdout.split('\n').forEach(line => {
				if (line.match(/^state\./)) {
					card = line.match(/^state.(\S+)/)[1]
				}
				if (line.match(/value true/) && device) {
					presents.push({card, device})
				}
				device = 0
				if (line.match(/hdmi/i)) {
					device = line.match(/pcm=(\d+)/)?.[1]
				}
			})
			return presents
		})()
	},

	get arrangements() {
		return arrangements
	},

	get devices() {
		return (async () => {return (await exec('LC_ALL=C aplay -l 2>/dev/null | grep "^card " | sed "s/card \\([0-9]\\+\\): .*, device \\([0-9]\\+\\).* \\[\\(.*\\)\\]/\\1 \\2 \\3/"')).stdout.split('\n').filter(_ => _)})()
	}
}

function updateActive(active) {
	let args = active.map(device => `"${device.name || ''}:${device.arrangement || ''}"`).join(' ')
	exec(`alsa-update-asound-conf.sh ${args}`).catch(() => {})
}

const arrangements = [
	{label: 'Stereo 2.0', value: '2'},
	{label: 'Surround 5.1', value: '6'},
	{label: 'Surround 7.1', value: '8'},
]
