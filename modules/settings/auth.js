const passwd = require('../passwd')
const fsp = require('../fsp')
const uuid = require('uuid').v4;

var auth = {}

module.exports = {
	_required: false,
	username: 'root',
	_tokenfile: '/tmp/settings_auth_token',

	set password(value) {
		if (this.check()) {
			passwd.changePassword(this.username, value || '', response => {
				if (response instanceof Error) {
					console.error(response)
				}
			})
		}
	},

	get required() {
		return this._required
	},
	set required(value) {
		if (this.check()) {
			this._required = value
			auth[this.$socket?.conn.id] = true
		}
	},

	get success() {
		return !this._required || this.$socket && auth[this.$socket.conn.id] || this.$request?.auth || !this.$socket && !this.$request && process.env.USER == 'root'
	},

	check() { 
		if (!this.success) {
			if (this.$socket) {
				this.$socket.emit('auth.check', false)
			} else {
				console.error('access denied')
			}
			return false
		}
		return true
	},

	login(password) {
		return new Promise(async (resolve, reject) => {
			if (this.$request && password && password == await this.token) {
				this.$request.auth = true
				resolve(true)
			}
			passwd.checkPassword(this.username, password || '', response => {
				if (response instanceof Error) {
					reject(response)
				} else if (!response) {
					// Wrong password
					resolve(false)
				} else if (this.$socket) {
					auth[this.$socket.conn.id] = true
				} else if (this.$request) {
					this.$request.auth = true
				}
				resolve(true)
			})
		})
	},

	logout() {
		auth[this.$socket?.conn.id] = false
	},

	get token() {
		return (async () => {return (await fsp.readFile(this._tokenfile, 'utf8').catch(() => ''))})()
	},

	get tokenfile() {
		return (async () => {
			if (!await fsp.exists(this._tokenfile)) {
				await fsp.writeFile(this._tokenfile, uuid())
				await fsp.chmod(this._tokenfile, 0o600)
			}
			return this._tokenfile
		})()
	}
}
