const exec = require('../exec')

module.exports = {
	sources: 'tvm tvsp vdr',
	merge: true,
	lines: 50,

	async update() {
		await exec.cmd('create-channelmap.sh', this.$socket)
		this.$root.services.restart('vdr-epg-daemon')
	}
}
