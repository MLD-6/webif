const axios = require('axios')
const exec = require('../exec')
const fs = require('fs')
const fsp = fs.promises
const lock = require('lock').Lock()

var progressOffset = 0
var installed = []
var flatpakPackages = []


module.exports = {
	updateCheck: true,

	async downloadConfig() {
		let configfile = '/tmp/config.tgz'
		await exec.cmd(`apt-config-backup.sh create ${configfile}`, this.$socket)
		return await fsp.readFile(configfile).catch(() => '')
	},

	async uploadConfig(config) {
		let configfile = '/tmp/config.tgz'
		await fsp.writeFile(configfile, config, 'binary')
		await exec.cmd(`apt-config-backup.sh restore ${configfile}`, this.$socket)
	},

	async installed() {
		let manual = await manualInstalled()
		let packages = (await exec('dpkg-query --show -f="\\${db:Status-Abbrev}\\${Package}\\t\\${Version}\\t\\${binary:Summary}\\n" | sed -n "s/ii //p"')).stdout.split('\n')
		if (installed.includes('flatpak')) {
			packages.push(...(await exec('flatpak list --app --columns=name,version,description,application | tail +1')).stdout.split('\n'))
		}
		return packages.map(package => {
			let [name, version, description, id] = package.split('\t')
			return {name, version, description, id: id || name, installed: true, manual: manual.includes(name), flatpak: !!id}
		}).filter(package => package.name).sort((a, b) => a.name.localeCompare(b.name))
	},

	async install(name) {
		if (this.$root.auth.check()) {
			await updateInstalled()
			let status = true
			let names = (Array.isArray(name) ? name : [name]).filter(name => !name.includes('.'))
			if (names.length) {
				let locale = await this.$root.system.locale
				if (locale && locale != 'C') {
					let filter = '^('+names.map(name => name+'-locale-'+locale.replace(/_.*/, '')).join('|')+')'
					names.push(...(await exec(`apt-cache search '${filter}'`)).stdout.split('\n').filter(_ => _).map(package => package.split(' - ')[0]))
				}
				names.push(...installed.filter(package => package.includes('-locale-') && names.includes(package.replace(/-locale-.+/, '-'))).map(package => package+'-'))
				try {
					await aptGet(this.$socket, ['install', ...names])
				} catch(e) {
					status = false
				}
			}
			names = (Array.isArray(name) ? name : [name]).filter(name => name.includes('.'))
			if (names.length && installed.includes('flatpak')) {
				try {
					await flatpak(this.$socket, ['install', ...names])
				} catch(e) {
					status = false
				}
			}
			updateInstalled(this.$socket)
			return status
		}
	},

	async installSystem(devices, collection=[]) {
		if (this.$root.auth.check() && devices) {
			try {
				return await installSystem(this.$root, this.$socket, devices, collection)
			} catch(e) {
				return false
			}
		}
	},

	async prefered() {
		let prefered = {...allPrefered}
		await updateInstalled()
		if (!installed.includes('vdr')) {
			delete(prefered['VDR-Plugins'])
			prefered['Server'] = prefered['Server'].filter(pkg => pkg != 'vdradmin-am')
		}
		for (var key in prefered) {
			prefered[key] = await list(prefered[key])
			if (!prefered[key].length) {
				delete(prefered[key])
			}
		}
		return prefered
	},

	async remove(name) {
		if (this.$root.auth.check()) {
			try {
				if (!name.includes('.')) {
					await updateInstalled()
					let names = installed.filter(package => package.replace(/-locale-.+/, '') == name)
					await aptGet(this.$socket, ['remove', ...names], 0.5)
					await aptGet(this.$socket, ['autoremove'], 1, false)
					updateInstalled(this.$socket)
				} else if (installed.includes('flatpak')) {
					await flatpak(this.$socket, ['uninstall', name], 1, false)
				}
				return true
			} catch(e) {
				updateInstalled(this.$socket)
				return false
			}
		}
	},

	async search(filter, full, page, pageSize=100) {
		return await search(filter, full, page, pageSize, this.$socket)
	},

	get sources() {
		return (async () => {
			if (!this._sources) {
				this._sources = [...new Set(['https://mld6.minidvblinux.de', ...(await fsp.readFile('/etc/apt/sources.list', 'utf8').catch(() => '')).match(/(https?:\/\/[^\/]+)/g)||[]])]
			}
			return this._sources
		})()
	},
	set sources(sources) {
		this._sources = sources
	},

	get srcNames() {
		return (async () => {
			let names = []
			for (let src of await this.sources) {
				for (let name of (await axios.get(`${src}/`).catch(_ => _)).data?.split('\n').map(line => line.match(/^<a href="(.*)\/">/)?.[1]) || []) {
					if (name && (await axios.get(`${src}/${name}/deb`).catch(() => {}))?.status == 200) {
						names.push(name)
					}
				}
			}
			return names
		})()
	},

	get srcName() {
		return new Promise(resolve => {
			lock('apt-get', async release => {
				let sources = await fsp.readFile('/etc/apt/sources.list', 'utf8').catch(() => '')
				for (let src of await this.sources) {
					let match = sources.match(RegExp(`${src.replaceAll('/', '\\/')}\/(.*)\/deb`))?.[1]
					if (match) {
						release(resolve)(match)
						break
					}
				}
			})
		})
	},
	set srcName(name) {
		if (name) {
			lock('apt-get', async (release) => {
				let sources = await fsp.readFile('/etc/apt/sources.list', 'utf8').catch(() => '')
				let newSrc = ''
				for (let src of await this.sources) {
					if ((await axios.get(`${src}/${name}/deb`).catch(() => {}))?.status == 200) {
						newSrc = `${src}/${name}/deb`
						break
					}
				}
				if (newSrc) {
					for (let src of await this.sources) {
						sources = sources.replaceAll(RegExp(`${src.replaceAll('/', '\\/')}\/.*\/deb`, 'g'), newSrc)
					}
					await fsp.writeFile('/etc/apt/sources.list', sources).catch(console.error).finally(release())
				}
			})
		}
	},

	async update() {
		try {
			await aptGet(this.$socket, ['update'], 0.1)
			flatpakPackages = []
			return true
		} catch(e) {
			return false
		}
	},

	async upgrade(name) {
		if (this.$root.auth.check()) {
			try {
				if (name) {
					if (!name.includes('.')) {
						await aptGet(this.$socket, ['install', '--only-upgrade', name], 0.9)
					} else if (installed.includes('flatpak')) {
						await flatpak(this.$socket, ['update', name], 0.9, false)
					}
				} else {
					await aptGet(this.$socket, ['install', '-f'], 0.6)
					await aptGet(this.$socket, ['dist-upgrade'], 0.8, false)
					if (installed.includes('flatpak')) {
						await flatpak(this.$socket, ['update'], 0.9, false)
					}
				}
				await aptGet(this.$socket, ['autoremove'], 1, false)
				updateInstalled(this.$socket)
				return true
			} catch(e) {
				updateInstalled(this.$socket)
				return false
			}
		}
	},

	get upgrades() {
		return (async () => {
			await aptGet(this.$socket, ['update'], 0, true, true).catch(() => {})
			let manual = await manualInstalled()
			let updates = (await exec('apt-get -q -s dist-upgrade | sed -n "s/Inst \\([^: ]*\\)[^ ]* \\(\\[\\([^ ]*\\)\\] \\)\\?(\\([^ ]*\\).*/\\1 \\3 \\4/p"')).stdout.split('\n').map(package => {
				let [name, oldVersion, newVersion] = package.split(' ')
				return {name: name, oldVersion: oldVersion, newVersion: newVersion, id: name, manual: manual.includes(name)}
			})
			if (installed.includes('flatpak')) {
				updates.push(...(await exec('flatpak remote-ls --updates --columns=name,version,application')).stdout.split('\n').map(package => {
					let [name, version, id] = package.split('\t')
					return {name: name, newVersion: version, id, manual: manual.includes(name), flatpak: true}
				}))
			}
			return updates.filter(package => package.name)
		})()
	},
}

async function manualInstalled() {
	await updateInstalled()
	let manual = (await exec('apt-mark showmanual')).stdout.split('\n').filter(name => name && !name.includes('-locale-') && !name.includes('-localedata-') && !autoInstalled.some(auto => name == auto))
	if (installed.includes('flatpak')) {
		manual.push(...(await exec('flatpak list --app --columns=name | tail +1')).stdout.split('\n'))
	}
	return manual
}

async function list(names, page) {
	return (await search('^('+names.join('|')+')$', true, page)).filter(package => names.includes(package.name.toLowerCase()))
}

async function search(filter, full, page, pageSize=100, socket) {
	page = page || 1
	if (socket?.searchController) {
		socket.searchController.abort()
	} else {
		await updateInstalled()
	}
	if (socket) {
		socket.searchController = new AbortController()
	}
	let packages = (await exec(`apt-cache -qqo "APT::Cache::Search::Version=2" search "${filter}"  | tr "\n" "\r" | sed "s/\r  / /g; s/\r\r/\r/g" | tr "\r" "\n" | while read n v a d; do echo "\${n%/*}\t$v\t$d"; done | ` + ( full ?  'cat' : 'grep -v "^lib\\|^kernel-\\|-locale-"' ) + ` | grep -v "\\-dbg\t\\|-dev\t\\|-doc\t\\|-src\t"`, {signal: socket?.searchController?.signal}).catch(() => {}))?.stdout.split('\n').map(package => {
		let [name, version, description] = package.split('\t')
		return {name, version, description: description?.replace(/^\[[^\]]*\]/, ''), id: name}
	}).filter(package => package.name) || []
	if (installed.includes('flatpak')) {
		flatpakPackages = flatpakPackages.length ? flatpakPackages : (await exec('flatpak search --columns=name,version,description,application . | uniq')).stdout.split('\n').map(package => {
			let [name, version, description, id] = package.split('\t')
			return {name, version, description, id, flatpak: true}
		}).filter(package => package.name)
		packages.push(...flatpakPackages.filter(package => RegExp(filter.toLowerCase()).test(package.name.toLowerCase())))
	}
	return packages.sort((a, b) => a.name.localeCompare(b.name)).slice((page-1)*pageSize, page*pageSize)
}

async function updateInstalled(socket) {
	if (socket || !installed.length) {
		installed = (await exec('dpkg-query --show -f="\\${db:Status-Abbrev}\\${Package}\\n" | sed -n "s/ii //p"')).stdout.split('\n')
		// if (installed.includes('flatpak')) {
		// 	installed.push(...(await exec('flatpak list --app --columns=application | tail +1')).stdout.split('\n'))
		// }
		if (socket) {
			socket.emit('packages.installed')
		}
	}
}

function aptGet(socket, args, maxProgress = 1, resetProgress = true, quiet = false) {
	return new Promise((resolve, reject) => {
		lock('apt-get', (release) => {
			var id = Date.now()
			var step = 0
			var steps = args[0] == 'update' ? 10 : 100
			var progress = 0
			var errors = []
			if (resetProgress) {
				progressOffset = 0
			}
			var action = ''
			var command = exec.spawn('apt-get', ['-q', '-y', '-o=Dpkg::Options::=--force-confdef', '-o=Dpkg::Options::=--force-confold', '-o=APT::Get::HideAutoRemove=1', ...args], {env: {DEBIAN_FRONTEND: 'noninteractive'}})
			var commandString = `# ${command.spawnargs.filter(arg => !arg.startsWith('-')).join(' ')}\n`
			if (!quiet) {
				socket?.emit('log', id, commandString)
				command.stdout.on('data', line => {
					let message = line.toString().replace(/([^\n\r]*\r)+([^\n]|$)/g, '$2')
					socket?.emit('log', id, message)
					let summary = message.match(/(\d+) upgraded, (\d+) newly installed, (\d+) to remove/)
					if (summary) {
						progressOffset += step / steps
						step = 0
						steps = summary[1]*5 + summary[2]*5 + summary[3]*1 + 3
					}
					let match = message.match(/(^|\n)(Reading|Building|Get|Hit|Ign|Selecting|Preparing|Unpacking|Setting up|Removing|Processing)\S* (\S+) (\S+) ?(\S+)?/)
					if (match) {
						step ++
						steps = step < steps ? steps : step + 1
						progress = progressOffset + step / steps * (maxProgress - progressOffset)
						if ((match[2] == 'Get' || match[2] == 'Hit') && match[5] == 'InRelease') action = 'Downloading updates'
						if (match[2] == 'Get' && match[5] != 'InRelease') action = `Downloading ${match[5]}`
						if (match[2] == 'Unpacking' || match[2] == 'Setting up' || match[2] == 'Removing') action = `${match[2]} ${match[3]}`
						socket?.emit("progress", progress, action)
					}
				})
			}
			command.stderr.on('data', line => {
				if (!line.toString().includes('delaying package configuration')) {
					quiet && socket?.emit('log', id, commandString)
					socket?.emit('log', id, line.toString(), true)
					errors.push(line.toString())
				}
			})
			command.on('close', code => {
				if (errors.length) {
					console.log(command.spawnargs.join(' '))
					console.log(errors.join(' '))
				}
				progressOffset = quiet ? progressOffset : progress
				release(code || errors.length ? reject : resolve)()
			})
		})
	})
}

function flatpak(socket, args, maxProgress = 1, resetProgress = true, quiet = false) {
	return new Promise((resolve, reject) => {
		lock('flatpak', (release) => {
			var id = Date.now()
			var step = 0
			var steps = args[0] == 'update' ? 10 : 100
			var progress = 0
			var errors = []
			if (resetProgress) {
				progressOffset = 0
			}
			var command = exec.spawn('flatpak', ['-y', '--noninteractive', ...args], {env: {LC_ALL: 'C'}})
			var commandString = `# ${command.spawnargs.filter(arg => !arg.startsWith('-')).join(' ')}\n`
			if (!quiet) {
				socket?.emit('log', id, commandString)
				command.stdout.on('data', line => {
					let message = line.toString().replace(/([^\n\r]*\r)+([^\n]|$)/g, '$2')
					socket?.emit('log', id, message)
					let match = message.match(/(^|\n)(\S+) (\S+)\/(\S+)\/(\S+)\/(\S+)/)
					if (match) {
						step ++
						steps = step < steps ? steps : step + 1
						progress = progressOffset + step / steps * (maxProgress - progressOffset)
						socket?.emit("progress", progress, `${match[1]} ${match[3]}`)
					}
				})
			}
			command.stderr.on('data', line => {
				quiet && socket?.emit('log', id, commandString)
				socket?.emit('log', id, line.toString(), true)
				errors.push(line.toString())
			})
			command.on('close', code => {
				if (errors.length) {
					console.log(command.spawnargs.join(' '))
					console.log(errors.join(' '))
				}
				progressOffset = quiet ? progressOffset : progress
				release(code || errors.length ? reject : resolve)()
			})
		})
	})
}

function installSystem(settings, socket, devices, collection) {
	return new Promise(async (resolve, reject) => {
		var id = Date.now()
		var errors = []
		let locale = await settings.system.locale
		if (locale && locale != 'C' && collection.length) {
			await exec('apt-get update')
			let filter = '^('+collection.map(name => name+'-locale-'+locale.replace(/_.*/, '')).join('|')+')'
			collection.push(...(await exec(`apt-cache search '${filter}'`)).stdout.split('\n').filter(_ => _).map(package => package.split(' - ')[0]))
		}
		var command = exec.spawn('install.sh', [
			'-s', devices?.system?.device?.value,
			...devices?.data?.device?.value ? ['-d', devices?.data?.device?.value] : [],
			...devices?.data?.delete ? ['--df'] : [],
			...devices?.data?.path?.value ? ['--dp', devices?.data?.path?.value] : [],
			...devices?.data?.copy ? ['--dc'] : [],
			'-i', collection.join(' ')
		])
		console.log(command.spawnargs.join(' '))
		socket?.emit('log', id, `# ${command.spawnargs.join(' ')}\n`)
		command.stdout.on('data', line => {
			let match = line.toString().match(/^(\d+)% ?(.*)/)
			if (match) {
				socket?.emit("progress", match[1] / 100, match[2])
			}
			socket?.emit('log', id, line.toString().replace(/^\d+% ?/, ''))
		})
		command.stderr.on('data', line => {
			if (!line.toString().includes('warning: setlocale:')) {
				socket?.emit('log', id, line.toString(), true)
				errors.push(line.toString())
			}
		})
		command.on('error', err => {
			socket?.emit('log', id, err.toString(), true)
			console.error(err)
			reject()
		})
		command.on('close', code => {
			if (errors.length) {
				console.error(command.spawnargs.join(' '))
				console.error(errors.join(' '))
			}
			if (code) {reject()} else {resolve(true)}
		})
	})
}

const allPrefered = {
	'Multimedia': ['vdr', 'kodi', 'squeezeplay', 'vlc', 'omxplayer', 'chromium'],
	'Server': ['lms', 'minisatip', 'fhem', 'jonglisto', 'owncloud', 'vdradmin-am', 'netdata'],
	'VDR-Plugins': ['vdr-plugin-dvd', 'vdr-plugin-femon', 'vdr-plugin-fritzbox', 'vdr-plugin-live', 'vdr-plugin-markad', 'vdr-plugin-mpv', 'vdr-plugin-text2skin', 'vdr-plugin-wirbelscan'],
	'Tools': ['filebrowser', 'mc', 'nano'],
	'Network': ['samba', 'nfs-server', 'nfs-client', 'vdr-addon-avahi-linker', 'wol'],
	'System': ['flatpak']
}

const autoInstalled = ['alsa-init', 'appstarter', 'apt', 'firmware-loader', 'install', 'kernel-modules', 'packagegroup-core-boot', 'packagegroup-core-ssh-openssh', 'packagegroup-x11-matchbox', 'plymouth', 'remoteconnection', 'snapshot', 'surf', 'tzdata', 'vboxguestdrivers', 'webif']
