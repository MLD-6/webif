const exec = require('../exec')
const fs = require('fs')
const fsp = fs.promises

module.exports = {
	get showAssistant() {
		return (async () => {return (this._showAssistant === undefined) ? await this.isLiveSystem : this._showAssistant})()
	},
	set showAssistant(value) {
		this._showAssistant = value
	},

	get isLiveSystem() {
		return (async () => {
			let rootFsType = (await fsp.readFile('/proc/mounts', 'utf8').catch(() => '')).split('\n').find(line => line.includes(' / ')).split(' ')[2]
			return rootFsType == 'overlay' || rootFsType == 'tmpfs'
		})()
	},

	get navigation() {
		return (async () => {
			let installed = (await exec('dpkg-query --show -f="\\${db:Status-Abbrev}\\${Package}\\n" | sed -n "s/ii //p"')).stdout.split('\n')
			if (await this.isLiveSystem) {
				installed.push('isLiveSystem')
			}
			let navigation = {...allNavigation}
			if (!this.$root.debug) {
				for (var key in navigation) {
					navigation[key] = navigation[key].filter(navigation => !navigation.package || installed.includes(navigation.package))
					if (!navigation[key].length) {
						delete(navigation[key])
					}
				}
			}
			return navigation
		})()
	},

	get status() {
		return (async () => {
			return {
				install: !await this.$root.install?.failed,
				firmware: !(await this.$root.firmware.missing)?.length,
				network: (await this.$root.system.ip)?.length,
				showAssistant: await this.showAssistant,
				url: await this.$root.system.url
			}
		})()
	},

	get version() {
		return (async () => {return (await exec('dpkg -s webif | sed -n "s/Version: //p"').catch(_ => _))?.stdout.trim()})()
	}
}

const allNavigation = {
	'Install': [
		{title: 'Assistant', package: 'isLiveSystem', icon: 'launch', page: '/assistant', needAuth: true},
	],
	'Tools': [
		{title: 'Apps', icon: 'apps', page: '/apps'},
		{title: 'EPG-Daemon', package: 'vdr-epg-daemon', icon: 'dvr', page: "/embed?nomargin#:9999/"},
		{title: 'Filebrowser', package: 'filebrowser', icon: 'folder_open', page: "/embed?nomargin#:8009/"},
		{title: 'LMS', package: 'lms', icon: 'music_video', page: "/embed?nomargin#:9000/"},
		{title: 'Mediathek HbbTV', package: 'mediathek-hbbtv', icon: 'movie', page: '/mediathekhbbtv'},
		{title: 'Mediathek View', package: 'vdr-plugin-mpv', icon: 'movie', page: '/mediathekview'},
		{title: 'Systeminfo', package: 'netdata', icon: 'show_chart', href: ":19999/"},
		{title: 'Terminal', package: 'shellinabox', icon: 'terminal', page: "/embed#:4488/"},
		{title: 'VDR OSD', package: 'vdr-plugin-restfulapi', icon: 'settings_remote', page: '/vdrosd'},
		{title: 'VDR Live', package: 'vdr-plugin-live', icon: 'live_tv', page: "/embed?nomargin#:8008/"},
		{title: 'VDR Mpv', package: 'vdr-plugin-mpv', icon: 'smart_display', page: '/vdrmpv'},
		{title: 'VDR Stream', package: 'vdr-plugin-streamdev-server', icon: 'smart_display', page: '/vdrstreamdev'},
	],
	'System': [
		{title: 'Information', icon: 'info', page: '/information'},
		{title: "Settings", icon: "settings", page: "/settings", needAuth: true},
		{title: "Packages", icon: "extension", page: "/packages", needAuth: true},
		{title: 'Services', icon: 'rule', page: '/services'},
		{title: 'Logs', icon: 'list', page: '/logs'},
		{title: 'Support', icon: 'bug_report', page: '/support'},
		{title: 'Map', icon: 'public', page: '/map', needAuth: true},
		{title: 'About', icon: 'help', page: '/about'},
	]
}
