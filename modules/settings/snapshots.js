const exec = require('../exec')

module.exports = {
	autoSnapshot: true,

	async all(device) {
		return (await exec(`snapshot -d "${device || ''}" list`).catch(e => {console.error(e.message)}))?.stdout.split('\n').filter(_ => _) || []
	},

	async create(device, name) {
		if (this.$root.auth.check() && name) {
			return await exec.cmd(`snapshot -d "${device || ''}" create "${name}"`)
		}
	},

	async restore(device, snapshot) {
		if (this.$root.auth.check() && snapshot) {
			return await exec.cmd(`snapshot -d "${device || ''}" restore "${snapshot}"`)
		}
	},

	async delete(device, snapshot) {
		if (this.$root.auth.check() && snapshot) {
			return await exec.cmd(`snapshot -d "${device || ''}" delete "${snapshot}"`)
		}
	}
}
