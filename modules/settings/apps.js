const exec = require('../exec')
const fsp = require('../fsp')
const ini = new (require('conf-cfg-ini'))({lineEnding: '\n', commentIdentifiers: ['#']})

module.exports = {
	async all() {
		let apps = []
		let dir = '/usr/share/menu/'
		let files = await fsp.readdir(dir).catch(() => {})
		for (let file of files) {
			let cast2type = (t) => {try {return JSON.parse(t)} catch(e){return t}}
			let content = await fsp.readFile(dir + file).catch(() => '')
			apps.push(...content.toString().replace(/\\\n/g, '').replace(/[\t ]+/g, ' ').replace(/.*\): */g, '').split('\n').filter(app => app).map(app => 
				Object.fromEntries(Array.from(app.matchAll(/(\S+)="([^"]*)"/g), match => [match[1], cast2type(match[2])]))
			))
		}

		dir = '/var/lib/flatpak/exports/share/applications/'
		// dir = '/usr/share/applications/'
		files = (await fsp.readdir(dir).catch(() => ([]))).filter(file => file.endsWith('.desktop'))
		for (let file of files) {
			let app = ini.decode(await fsp.readFile(dir + file, 'utf8').catch(() => ''))['Desktop Entry']
			apps.push({title: app.Name, description: app.Comment, section: app.Categories.replace(';', '/').slice(0,-1), needs: app.Terminal == 'true' ? 'text' : 'x11', command: app.Exec.replace(/@@.*/, '')})
		}

		let app = apps.find(app => app.title == this.active)
		if (app) {
			app.active = true
			app.running = (await this.$root.services?.status('appstarter'))?.running
		}
		return apps
	},

	get app() {
		return (async () => {
			let apps = await this.all()
			let name = apps.find(app => app.title == this.active)?.title || await this.autostart
			return apps.find(app => app.title == name)
		})()
	},

	get autostart() {
		return (async () => {
			let apps = await this.all()
			let app = apps.find(app => app.title == this._autostart)
			if (!app && this._autostart !== false) {
				app = apps.filter(app => app.autostart).sort((a, b) => b.autostart - a.autostart)?.[0]
			}
			return app?.title
		})()
	},
	set autostart(value) {
		if (this.$root.auth.check()) {
			this._autostart = value
		}
	},

	async start(name) {
		this.active = name
		return await exec.cmd(`systemctl restart appstarter`, this.$socket)
	},

	async restart() {
		return await exec.cmd(`systemctl restart appstarter`, this.$socket)
	},

	async stop() {
		delete this.active
		return await exec.cmd(`systemctl stop appstarter`, this.$socket)
	}
}
