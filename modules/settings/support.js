const exec = require('../exec')
const fetch = require('node-fetch')
const fs = require('fs')
const fsp = fs.promises

module.exports = {
	get isOpen() {
		return (async () => {return Object.fromEntries((await exec(`systemctl show remoteconnection | cat`)).stdout.split('\n').map(state => state.split('='))).SubState == 'running'})()
	},

	get isEnabled() {
		return (async () => {return Object.fromEntries((await exec(`systemctl show remoteconnection | cat`)).stdout.split('\n').map(state => state.split('='))).UnitFileState == 'enabled'})()
	},

	async openConnection(port) {
		if (this.$root.auth.check()) {
			this.port = port || 2200 + Math.round(Math.random() * 99)
			return await exec.cmd('systemctl start remoteconnection', this.$socket)
		}
		return false
	},

	async closeConnection() {
		if (this.$root.auth.check()) {
			delete this.$root.support.port
			return await exec.cmd('systemctl stop remoteconnection', this.$socket)
		}
		return false
	},

	async uploadLog() {
		let logfile = '/tmp/log.tgz'
		await exec.cmd(`supportlog.sh ${logfile}`, this.$socket)
		return (await (await fetch(`${this.$root.mldUrl}/site/log.php`, {method: 'POST', body: await fsp.readFile(logfile).catch(() => '')})).text()).trim()
	},

	async downloadLog() {
		let logfile = '/tmp/log.tgz'
		await exec.cmd(`supportlog.sh ${logfile}`, this.$socket)
		return await fsp.readFile(logfile).catch(() => '')
	}
}
