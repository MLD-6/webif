const exec = require('../exec')
const lock = require('lock').Lock()

module.exports = {
	get all() {
		return (async () => {
			if (this.$root.debug) {
				return [...allServices]
			}
			let services = []
			for(var service of allServices) {
				service = await getStatus({...service})
				if (service) {
					if (!this.$root.auth.success) {
						service.commands = []
					}
					if (service.href) {
						let port = (await this.$root[service.service]?.port) || service.port
						service.href = port ? (':' + port + service.href) : service.href
					}
					services.push(service)
				}
			}
			return services
		})()
	},

	async status(service) {
		return await getStatus({service})
	},

	async exec(name, cmd) {
		if (this.$root.auth.check()) {
			const service = allServices.find(service => service.name == name)
			let command = service.commands?.find(command => command.name == cmd)?.command
			command = !command && service.service ? `systemctl ${cmd} ${service.service}` : (!command && service.docker ? `docker ${cmd} ${service.docker}` : command)
			await exec.cmd(command, this.$socket)
			return await getStatus(service)
		}
	},

	async start(service) {
		if (this.$root.auth.check()) {
			if (allServices.find(s => s.docker == service)) {
				return await exec.cmd(`docker start ${service}`, this.$socket)
			} else {
				return await exec.cmd(`systemctl start ${service}`, this.$socket)
			}
		}
	},

	async restart(service) {
		if (this.$root.auth.check()) {
			if (service == 'webif') {
				return await new Promise(resolve => {
					lock('apt-get', release => {
						release(resolve)(true)
						exec.cmd(`systemctl restart ${service}`, this.$socket)
					})
				})
			} else {
				if (allServices.find(s => s.docker == service)) {
					return await exec.cmd(`docker restart ${service}`, this.$socket)
				} else {
					return await exec.cmd(`systemctl restart ${service}`, this.$socket)
				}
			}
		}
	},

	async stop(service) {
		if (this.$root.auth.check()) {
			if (allServices.find(s => s.docker == service)) {
				return await exec.cmd(`docker stop ${service}`, this.$socket)
			} else {
				return await exec.cmd(`systemctl stop ${service}`, this.$socket)
			}
		}
	}
}

async function getStatus(service) {
	if (service.service) {
		service.running = false
		service.enabled = false
		service.description = ''
		try {
			let status = Object.fromEntries((await exec(`systemctl show ${service.service}`)).stdout.split('\n').map(state => state.split('=')))
			service.running = status.SubState == 'running'
			service.enabled = status.UnitFileState == 'enabled'
			service.description = status.Description
			if (status.LoadState == 'not-found') {
				return
			}
		} catch(e) {
			return
		}
		service.commands = []
		if (service.start !== false) {
			service.commands = service.commands.concat(service.running ? [{name: 'stop'}, {name: 'restart'}] : [{name: 'start'}])
		}
		service.commands = service.commands.concat(service.enabled ? [{name: 'disable'}] : [{name: 'enable'}])
	}
	if (service.docker) {
		service.running = false
		service.description ||= ''
		try {
			let running = (await exec(`LC_ALL=C docker inspect --format '{{.State.Running}}' ${service.docker} 2>&1`)).stdout.trim()
			service.running = running == 'true'
			if (running.includes('no such object')) {
				return
			}
		} catch(e) {
			return
		}
		service.commands = service.running ? [{name: 'stop'}, {name: 'restart'}] : [{name: 'start'}]
	}
	return service
}

const allServices = [
	{name: 'System', running: true, commands: [{name: 'reboot', command: 'reboot'}, {name: 'poweroff', command: 'poweroff'}]},
	{name: 'EPG-Daemon', docker: "vdr-epg-daemon"},
	{name: 'Filebrowser', service: "filebrowser"},
	{name: 'Hyperion', service: "hyperion", port: 8090, href: '/'},
	{name: 'LCDd', service: "lcdd"},
	{name: 'LCDproc', service: "lcdproc"},
	{name: 'LMS', docker: "lms"},
	{name: 'MiniSatIP', service: "minisatip", port: 8080, href: '/'},
	{name: 'Network', service: "systemd-networkd"},
	{name: 'Oscam', service: "oscam", port: 8006, href: '/'},
	{name: 'Remote connection', service: "remoteconnection"},
	{name: 'Samba', service: "smb"},
	{name: 'Shellinabox', service: "shellinabox"},
	{name: 'SSH', service: "sshd@*", start: false},
	{name: 'Timesync', service: "systemd-timesyncd"},
	{name: 'VDR', service: "vdr"},
	{name: 'Webif', service: "webif"},
	{name: 'Xserver', service: "xserver-nodm"},
]