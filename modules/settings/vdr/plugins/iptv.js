const dir = require('node-dir');
const fsp = require('../../../fsp')

module.exports = {
	count: 1,

	async getM3uLists() {
		return (await dir.promiseFiles('/etc/vdr/plugins/iptv').catch(() => []))?.map(c => c.replace(/.*\//, '')).filter(c => c.match(/\.m3u$/)).map(c => c.replace(/\.m3u$/, ''))
	},

	async getM3uList(name) {
		return await fsp.readFile(`/etc/vdr/plugins/iptv/${name}.m3u`, 'utf8').catch(() => '')
	},

	async deleteM3uList(name) {
		if (this.$root.auth.check()) {
			await fsp.unlink(`/etc/vdr/plugins/iptv/${name}.m3u`)
			return true
		}
		return false
	},

	async setM3uList(name, list) {
		if (this.$root.auth.check() && name) {
			await fsp.mkdir('/etc/vdr/plugins/iptv', {recursive: true})
			await fsp.writeFile(`/etc/vdr/plugins/iptv/${name}.m3u`, list)
			return true
		}
		return false
	},

	get ytdlp() {
		return (async () => {return !!(await this.$root.packages.installed()).find(pkg => pkg.name == 'yt-dlp')})()
	},

	set ytdlp(aktive) {
		(async () => {
			if (this.$root.auth.check()) {
				if (aktive) {
					this.$root.packages.install('yt-dlp')
				} else {
					this.$root.packages.remove('yt-dlp')
				}
			}
		})()
	},
}
