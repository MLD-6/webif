const exec = require('../../../exec')

module.exports = {
	country: 82,
	satellite: 80,
	freeOnly: true,

	get countries() {
		return (async () => {
			return (await exec('svdrpsend.sh plug wirbelscan lstc | sed -n "s/^900.//p"')).stdout.split('\n').filter(_ => _).map(line => (([id, lc, name]) => ({ id, lc, name: name.trim() }))(line.split(':')))
		})()
	},

	get satellites() {
		return (async () => {
			return (await exec('svdrpsend.sh plug wirbelscan lsts | sed -n "s/^900.//p"')).stdout.split('\n').filter(_ => _).map(line => (([id, code, name]) => ({ id, code, name: name.trim() }))(line.split(':')))
		})()
	},

	async scan() {
		let flags = 1 + 2 + 4 + (this.freeOnly ? 8 : 0) + 16
		await exec.cmd(`svdrpsend.sh PLUG wirbelscan setup 3:0:${this.type || 0}:0:0:0:0:${this.country || 0}:${this.satellite || 0}:1:0:${flags} | sed -n "s/^900.//p"`, this.$socket)
		await exec.cmd('svdrpsend.sh plug wirbelscan s_start | sed -n "s/^900.//p"', this.$socket)
	},

	get status() {
		return (async () => {
			return (await exec('svdrpsend.sh plug wirbelscan query | sed -n "s/^900.current //p"')).stdout.split('\n').filter(_ => _).reduce((obj, item) => {
				const [key, ...values] = item.split(':')
				obj[key.trim()] = values.join(':').trim()
				return obj
			}, {})
		})()
	},

	async stop() {
		await exec.cmd('svdrpsend.sh PLUG wirbelscan s_stop', this.$socket)
	},

	async sort() {
		await this.$root.vdr.setChannels(this.$root.vdr.active, (await this.$root.vdr.sortChannels(this.$root.vdr.getChannels().split('\n'))).join('\n'))
	},

	get types() {
		return Object.entries(types).map(([id, name]) => ({id, name}))
	}
}

const types = {
		0: 'DVB-T',
		1: 'DVB-C',
		2: 'DVB-S',
		5: 'ATSC'
}
