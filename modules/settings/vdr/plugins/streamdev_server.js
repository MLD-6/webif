const fetch = require('node-fetch')

module.exports = {
	async playlist(format, recordings, path) {
		let url = ''
		let uri = ''
		if (this.$request && !format) {
			format = this.$request.query.format
			recordings = this.$request.query.recordings
			path = this.$request.query.path
			url = `http://${this.$request.headers.host}`
			uri = this.$request.originalUrl.replace(/\?.*/, '')
		} else {
			url = await this.$root.system.url
			uri = '/setting/vdr.plugins.streamdev_server.playlist()'
		}

		let playlist = ['#EXTM3U']
		if (recordings) {
			let list = (await (await fetch(`${await this.$root.system.url}:3000/${format}/recordings.m3u`).catch(_=>_)).text()).split('\n')
			if(path) {
				let title = ''
				list.forEach(line => {
					if (title) {
						playlist.push(cleanupTitle(title.replace(`${path}~`, '')))
						playlist.push(line)
					}
					title = line.includes('#EXTINF') && line.includes(`${path}~`) ? line : ''
				})
			} else {
				[...new Set(list.filter(line => line.includes('~')).map(line => line.replace(/.*,\S+ \S+ \S+ ([^~]+)~.*/, '$1')))].forEach(path => {
					playlist.push(`#EXTINF:-1,->${cleanupTitle(path)}`)
					playlist.push(`${url}${uri}?format=${format}&recordings=1&path=${path.replace(/ /g, '%20')}`)
				})
				let title = ''
				list.forEach(line => {
					if (title) {
						playlist.push(cleanupTitle(title))
						playlist.push(line)
					}
					title = line.includes('#EXTINF') && !line.includes('~') ? line : ''
				})
			}
		} else {
			let list = (await (await fetch(`${await this.$root.system.url}:3000/${format}/groups.m3u`).catch(_=>_)).text()).split('\n')
			if(!list.find(line => line.includes('#EXTINF'))) {
				playlist.push('#EXTINF:-1,->Channels')
				playlist.push(`${url}:3000/${format}/channels.m3u`)
			} else {
				playlist = list
			}
			playlist.push('#EXTINF:-1,->Recordings')
			playlist.push(`${url}${uri}?format=${format}&recordings=1`)
		}
		return playlist.join('\n')
	},
}

function cleanupTitle(title) {
	return title
		.replace(/,/, ';').replace(/ *, */g, ' ').replace(/;/, ',')
		.replace(/-1,/, '+1,').replace(/-/g, '--').replace(/\+1,/, '-1,')
}