const fetch = require('node-fetch')

module.exports = {
	async query(query) {
		let result = await (await fetch('https://mediathekviewweb.de/api/query', {
			method: 'POST',
			headers: {'Content-Type': 'text/plain'},
			body: JSON.stringify(query)
		}).catch(() => {}))
		return result?.status == 200 ? result.json() : {result: {results: []}}
	},

	async play(url) {
		if (url) {
			if (await this.$root.apps.active == 'VDR') {
				await this.$root.vdr.plugins.mpv.play(url)
			} else {
				await this.$root.vdr.plugins.mpv.set(url)
				await this.$root.apps.start('VDR')
			}
		}
	},
}
