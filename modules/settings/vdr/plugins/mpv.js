const exec = require('../../../exec')
const fsp = require('../../../fsp')
const playlist = '/var/spool/mpv.playlist'

module.exports = {
	decoder: 'gpu',

	get decoders() {
		return ['vdpau', 'vaapi', 'gpu', 'x11']
	},

	async play(url) {
		await this.set(url)
		if (await fsp.exists(playlist)) {
			await exec.cmd(`svdrpsend.sh PLUG mpv PLAY ${playlist}`, this.$socket)
		}
	},

	async set(url) {
		if (url) {
			await fsp.writeFile(playlist, url+'\n')
		}
	},

	async add(url) {
		if (url) {
			await fsp.appendFile(playlist, url+'\n')
		}
	},

	async get() {
		return (await fsp.readFile(playlist, 'utf8').catch(() => '')).split('\n').filter(_ => _)
	},

	async clear() {
		await fsp.unlink(playlist).catch(_ => _)
	}
}
