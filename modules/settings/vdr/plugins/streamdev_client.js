const asyncScanLocal = require('../../../portscan').asyncScanLocal
const exec = require('../../../exec')
const fs = require('fs')
const fsp = fs.promises

module.exports = {
	wait_for_server: false,

	async servers() {
		return await asyncScanLocal(2004)
	},

	async setServerChannellist(enable) {
		if (this.$root.auth.check()) {
			const channellist = 'Server-channellist'
			if (enable) {
				fsp.mkdir('/etc/vdr/channels', {recursive: true}).then(() => {
					fsp.appendFile(`/etc/vdr/channels/${channellist}.conf`, '')
				})
				if (!this.$root.vdr.channels.active) {
					this.$root.vdr.channels.active = channellist
				}
			} else {
				fsp.unlink(`/etc/vdr/channels/${channellist}.conf`).catch(() => {})
				if (this.$root.vdr.channels.active == channellist) {
					this.$root.vdr.channels.active = ''
				}
			}
		}
	},

	get server_ip() {
		return this._server_ip
	},
	set server_ip(ip) {
		this._server_ip = ip
		this.update_server_mac()
	},

	get start_server() {
		return this._start_server || false
	},
	set start_server(enable) {
		this._start_server = enable
		if (enable && !this._server_mac) {
			this.update_server_mac()
		}
	},

	get server_mac() {
		return (async () => {
			if (!this._server_mac && this._server_ip) {
				await this.update_server_mac()
			}
			return this._server_mac
		})()
	},
	set server_mac(mac) {
		this._server_mac = mac
	},
	async update_server_mac() {
		this._server_mac = ''
		if (this._server_ip) {
			await exec(`ping -c 1 -W 1 '${this._server_ip}'`).catch(() => {})
			this._server_mac = (await fsp.readFile('/proc/net/arp', 'utf8').catch(() => '')).split('\n').find(line => line.startsWith(this._server_ip+' '))?.replace(/.* (\S+:\S+) .*/, '$1')
		}
	}
}
