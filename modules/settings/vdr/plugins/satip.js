const upnp = require('node-upnp-utils')

module.exports = {
	get args() {
		return (async () => {return getDeviceConfig(this.devices) + (this._args ? ' '+this._args : '')})()
	},
	set args(value) {
		if (this.$root.auth.check()) {
			this._args = value
		}
	},

	async servers() {
		return new Promise((resolve, reject) => {
			let servers = []
			try {
				upnp.on('added', (device) => {
					if (device?.description?.device?.deviceType.includes('SatIPServer')) {
						servers.push({ip: device.address, name: device.description.device.friendlyName})
					}
				})
				upnp.on('error', e => {
					if (e.code == 'EADDRINUSE') {
						upnp.discovery_started = false
						resolve([{name: 'No scan, upnp is in use'}])
					}
				})
				upnp.startDiscovery()
				setTimeout(() => {
					try {
						upnp.stopDiscovery()
					} catch(error) {}
					resolve(servers)
				}, 3000)
			} catch (error) {
				resolve(servers)
			}
		})
	}
}

function getDeviceConfig(devices) {
	let count = 0
	let config = []
	devices?.forEach((device, index) => {
		if (device.server && device.type && device.count) {
			count += device.count
			config.push(`${device.server}|DVB${device.type}-${device.count}|Server${index}`)
		}
	})
	if (config.length) {
		return `-d ${count} -s ${config.join(';')}`
	} else if (devices?.[0]?.count) {
		return `-d ${devices[0].count}`
	} else {
		return ''
	}
}
