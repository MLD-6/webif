const exec = require('../../../exec')
const fetch = require('node-fetch')
const fsp = require('../../../fsp')

module.exports = {
	port: 8002,
	host: 'localhost',
	// host: '192.168.178.5',

	async info() {
		let status = {}
		let response = await fetch(`http://${this.host}:${this.port}/info.json`).catch(_=>_)
		status.info = response.status == 200 ? await response.json() : {message: 'Connection error'}
		response = await fetch(`http://${this.host}:${this.port}/osd.json`).catch(_=>_)
		let data = response.status == 200 ? await response.json() : {message: 'Connection error'}
		status.program = data.ProgrammeOsd
		status.menu = !data.TextOsd?.message && data.TextOsd
		status.message = data.TextOsd?.message
		return status
	},

	async remote(key) {
		let response = await fetch(`http://${this.host}:${this.port}/remote/${key}`, {method: 'POST'}).catch(_=>_)
		return response.status == 200
	},

	async screenshot(width, height) {
		const quality = 60
		const image = '/data/photo/vdr/restfulapi.jpg'
		await exec(`svdrpsend.sh GRAB ${image} ${quality} ${width||1920} ${height||1080}`).catch(_=>_)
		if (fsp.exists(image)) {
			return (await fsp.readFile(image).catch(() => null))
		}
		return null
	}
}
