const fs = require('fs')
const fsp = fs.promises

module.exports = {
	get active() {
		return this._active
	},
	set active(value) {
		if (this.$root.auth.check()) {
			(async () => {this._active = await fsp.writeFile('/etc/vdr/scr.conf', value ? configs[value] : '').then(() => value).catch(() => '')})()
		}
	},

	get configs() {
		return Object.keys(configs)
	},

	async getConfig() {
		return await fsp.readFile(`/etc/vdr/scr.conf`, 'utf8').catch(() => '')
	},
	async setConfig(name, config) {
		if (this.$root.auth.check() && name) {
			this._active = await fsp.writeFile('/etc/vdr/scr.conf', config).then(() => name).catch(() => '')
			return true
		}
	}
}

var configs = {
	'DUR-LINE, Inverto Black': `
		0 1210
		1 1420
		2 1680
		3 2040
	`,
	'DUR-LINE (UCP18), DCT-DELTA, TECHNISAT, SMART': `
		0 1284
		1 1400
		2 1516
		3 1632
		4 1748
		5 1864
		6 1980
		7 2096
	`,
	'DUR-LINE (VDU), JULTEC, AXING': `
		0 1280
		1 1382
		2 1484
		3 1586
		4 1688
		5 1790
		6 1892
		7 1994
	`,
	'DUR-LINE (UK124), Gigablue (Ultra SCR-LNB)': `
		0 1210
		1 1420
		2 1680
		3 2040
		4 1005
		5 1050
		6 1095
		7 1140
		8 1260
		9 1305
		10 1350
		11 1475
		12 1520
		13 1565
		14 1610
		15 1725
		16 1770
		17 1815
		18 1860
		19 1905
		20 1950
		21 1995
		22 2085
		23 2130
	`,
	'Durline (UK119), FTE (HQ dCSS)': `
		0 1076
		1 1178
		2 1280
		3 1382
		4 1484
		5 1586
		6 1688
		7 1790
		8 975
		9 1025
		10 1225
		11 1330
		12 1430
		13 1535
		14 1630
		15 1730
		16 1840
		17 1880
		18 1920
		19 1960
		20 2000
		21 2040
		22 2080
		23 2125
	`,
	'FTE': `
		0 1400
		1 1632
		2 1284
		3 1516
		4 1864
		5 2096
		6 1748
		7 1980
	`,
	'INVERTO, ETRONIX': `
		0 1680
		1 1420
		2 2040
		3 1210
	`,
	'INVERTO (IDLP-UST110-CU010-BPP)': `
		0 1076
		1 1178
		2 1280
		3 1382
		4 1484
		5 1586
		6 1688
		7 1790
	`,
	'INVERTO (IDLU-UWT110-CUD10-32P)': `
		0 1210
		1 1420
		2 1680
		3 2040
		4 984
		5 1020
		6 1056
		7 1092
		8 1128
		9 1164
		10 1156
		11 1292
		12 1325
		13 1364
		14 1458
		15 1494
		16 1530
		17 1566
		18 1602
		19 1638
		20 1716
		21 1752
		22 1788
		23 1824
		24 1850
		25 1896
		26 1932
		27 1968
		28 2004
		29 2076
		30 2112
		31 2148
	`,
	'KATHREIN (UAS481, EXR 501, EXR 551, EXR 552)': `
		0 1400
		1 1516
		2 1632
		3 1748
	`,
	'KATHREIN (EXR 1542, EXR 2542, EXR 1942, EXR 2942)': `
		0 1284
		1 1400
		2 1516
		3 1632
	`,
	'KATHREIN (EXR 1581, EXR 2581, EXU 908)': `
		0 1284
		1 1400
		2 1516
		3 1632
		4 1748
		5 1864
		6 1980
		7 2096
	`,
	'SKYTRONIC, PREISNER, WISI': `
		0 1178
		1 1280
		2 1382
		3 1484
		4 1586
		5 1688
		6 1790
		7 1892
	`,
}
configs = Object.fromEntries(Object.entries(configs).map(([k, v]) => [k, v.replace(/^\s+/gm, '')]))
