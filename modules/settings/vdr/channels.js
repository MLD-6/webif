const dir = require('node-dir');
const fs = require('fs')
const fsp = fs.promises
const fetch = require('node-fetch')
const dateTime = require('node-datetime')

module.exports = {
	async getLocalLists() {
		return (await dir.promiseFiles('/etc/vdr/channels').catch(() => []))?.map(c => c.replace(/.*\//, '')).filter(c => c.match(/^[^.].*\.conf$/)).map(c => c.replace(/\.conf/, ''))
	},

	async getOnlineLists() {
		let channellists = {'Top': {}}
		if (this.$root.system.id) {
			let names = (await (await fetch(`${this.$root.mldUrl}/site/channel.php?channellists&id=${this.$root.system.id}`)).text()).split('\n').filter(name => name.length>3)
			for(let fullname of names) {
				let [_, type, address] = fullname.match(/(S|[^S]\S?) ?(.*)/)
				channellists['Top'][`DVB-${type} ${address.replaceAll(',', ' - ')}`] = fullname
			}
		}
		let names = (await (await fetch(`${this.$root.mldUrl}/site/channel.php?channellists`)).text()).split('\n').filter(name => name.length>3)
		for(let fullname of names) {
			let [_, type, address] = fullname.match(/(S|[^S]\S?) ?(.*)/)
			let names = ['DVB-'+type, ...address.split(',')]
			let channellist = channellists
			while (names.length > 1) {
				let name = names.shift()
				channellist[name] = typeof channellist[name] == 'object' ? channellist[name] : {}
				channellist = channellist[name]
			}
			channellist[names.shift()] = fullname
		}
		return channellists
	},

	async setOnlineList(name) {
		if (this.$root.auth.check() && name?.match(/(\S+) (.*)/)) {
			let [_, type, address] = name.match(/(\S+) (.*)/)
			let channels = (await (await fetch(`${this.$root.mldUrl}/site/channel.php?channellist&type=${type}&address=${address}`)).text()).split('\n')
			if (!channels.length) {
				return false
			}
			name = 'DVB-' + name.replace(/,/g, ' - ')
			let date = dateTime.create().format('Y-m-d_H:M:S')
			await fsp.mkdir('/etc/vdr/channels', {recursive: true})
			await fsp.rename(`/etc/vdr/channels/${name}.conf`, `/etc/vdr/channels/${name} ${date}.conf`).catch(() => {})
			await fsp.writeFile(`/etc/vdr/channels/${name}.conf`, (await this.sortChannels(channels)).join('\n'))
			this.active = name
			return true
		}
		return false
	},

	async deleteList(name) {
		if (this.$root.auth.check()) {
			await fsp.unlink(`/etc/vdr/channels/${name}.conf`)
			if (name == this.active) {
				this.active = ''
			}
			return true
		}
		return false
	},

	async getChannels() {
		return await fsp.readFile(`/etc/vdr/channels/${this.active}.conf`, 'utf8').catch(() => '')
	},

	async setChannels(name, channels) {
		if (this.$root.auth.check() && name) {
			await fsp.mkdir('/etc/vdr/channels', {recursive: true})
			await fsp.writeFile(`/etc/vdr/channels/${name}.conf`, channels)
			this.active = name
			return true
		}
		return false
	},

	async sortChannels(channels, locale) {
		let channelsSorted = []
		let group = ''
		;(channelsOrder[locale || (await this.$root.system.locale)?.replace(/_.*/, '')] || Object.values(channelsOrder)[0]).forEach(name => {
			if (name.startsWith(':')) {
				group = name
			} else {
				let channelsByName = []
				channelsByName = channels.filter(channel => channel.toLowerCase().startsWith(`${name} hd;`))
				channelsByName = channelsByName.length ? channelsByName : channels.filter(channel => channel.toLowerCase().startsWith(`${name};`))
				channelsByName = channelsByName.length ? channelsByName : channels.filter(channel => channel.toLowerCase().startsWith(`${name}`))
				if (channelsByName.length) {
					if (group) {
						channelsSorted.push(group)
						group = ''
					}
					channelsSorted.push(...channelsByName)
				}
			}
		})
		if (group && channelsSorted.length) {
			channelsSorted.push(group)
		}
		channels.forEach(channel => {
			channelsSorted.includes(channel) || channelsSorted.push(channel)
		})
		return channelsSorted
	}
}

const channelsOrder = {
	de: [':->Favoriten', 'das erste', 'zdf', 'das vierte', 'rtl television', 'sat.1', 'prosieben', 'kabel eins', 'rtl2', 'vox', 'tele 5', 'dmax', 'anixe sd', 'einsplus', 'tagesschau24', 'einsfestival', 'br-alpha', 'arte', 'zdf_neo', 'zdfinfo', 'zdf.kultur', '3sat', 'radio bremen tv', 'servustv deutschland', 'nick/comedy', 'nick/cc aut', 'sixx', 'deutsches gesundheitsfernsehen', 'orf2e', 'lt1-ooe', 'tvm/wwtv', 'tv trwam', 'erf 1', 'schau tv', 'holiday', 'a.tv', 'tirol tv', 'franken fernsehen', 'franken sat', 'lokal sat', 'mütv/rfo', 'ontv regional', 'ftl deutschland', 'ric', 'volks tv', 'anixe itv', 'rtlnitro', ':->Kinderprogramme', 'kika', 'super rtl', 'viva germany', ':->News', 'n-tv', 'n24', 'phoenix', 'cnbc europe', 'bbc world', 'cnn int.', 'euronews', 'sky news intl', 'deutsches wetterfernsehen', ':->Music', 'im1', 'gotv', 'magicstar digitalradio promo', 'deluxe music', 'melodie express', ':->Sport', 'sport1', 'eurosport deutschland', 'renault tv', ':->Kirche', 'god channel', 'k-tv', 'bibel tv', 'hope channel deutsch', 'die neue zeit tv', 'ewtn katholisches tv', 'sophia tv', ':->Sky', 'sky select', ':->Regional Programme', 'bayerisches fs nord', 'bayerisches fs süd', 'hr-fernsehen', 'mdr sachsen', 'mdr s-anhalt', 'mdr thüringen', 'ndr fs hh', 'ndr fs mv', 'ndr fs nds', 'ndr fs sh', 'rbb berlin', 'rbb brandenburg', 'sr fernsehen', 'swr fernsehen bw', 'swr fernsehen rp', 'wdr aachen', 'wdr bielefeld', 'wdr bonn', 'wdr dortmund', 'wdr duisburg', 'wdr düsseldorf', 'wdr essen', 'wdr köln', 'wdr münster', 'wdr siegen', 'wdr test a', 'wdr wuppertal', 'sat.1 hh/sh', 'sat.1 ns/bremen', 'sat.1 rhlpf/hessen', 'sat.1 bayern', 'sat.1 nrw', ':->Shopping', 'sonnenklar tv', 'hse24 extra', 'hse24', 'qvc deutschland', 'qvc beauty', 'best of shopping', 'beauty tv', 'channel 21', 'meintvshop', '1-2-3.tv', 'blucom demokanal', 'regio tv', 'mediashop- meine einkaufswelt', 'mediaspartv homeshopping', 'rnf', 'rtn myestate', 'jml shop', 'collection', 'channel21', 'pearl.tv', 'dhd24.tv', 'juwelo tv', 'rhein main tv', 'mediashop- neuheiten', 'astra vision', 'fashion one', 'hse24 trend', 'qvc plus', 'starparadies at', 'channel21 express', 'volksmusik', 'cashtv', 'informatiekanaal', 'aegypten.tv', 'starparadies d', 'ojom tv', 'rtl tele letzebuerg', 'dhd24 plus', 'dr.dishtv-welt der technik', 'shop24direct', ':->Radio', 'swr3', 'bayern 3', 'n-joy', 'sunshine live', 'antenne bayern', 'on3-radio', 'fritz', 'dkultur', 'dlf', 'rock antenne', 'hit radio ffh', 'planet radio', 'oe3', 'inselradio', 'radio top40', 'erf plus', 'teddy', 'domradio', 'egofm', 'klassik radio', 'harmony.fm', 'hitradio oe3', '1live diggi', '1live', 'antenne brandenburg', 'b5 aktuell', 'b5 plus', 'br verkehr', 'bayern 1', 'bayern 2', 'br-klassik', 'bayern 4 klassik', 'bayern plus', 'bremen eins', 'bremen vier', 'dasding', 'inforadio', 'mdr figaro', 'mdr info', 'mdr jump', 'mdr sputnik', 'mdr1 sa-anhalt', 'mdr1 sachsen', 'mdr1 thÜringen', 'mehrkanaltest', 'ndr 1 nieders.', 'ndr 1 radio mv', 'ndr1wellenord', 'ndr 2', 'ndr 90', 'ndr info spez.', 'ndr info', 'ndr kultur', 'nordwestradio', 'sr1 europawelle', 'sr2 kulturradio', 'sr3 saarlandwelle', 'swr1 bw', 'swr1 rp', 'swr2', 'swr4 bw', 'swr4 rp', 'kiraka', 'wdr 1', 'wdr 2', 'wdr 3', 'wdr 4', 'wdr 5', 'wdr event', 'wdr funkhaus europa', 'you fm', 'swrinfo', 'hr info +', 'hr-info', 'hr1', 'hr2', 'hr3', 'hr4', 'kulturradio', 'radioberlin 88', 'radioeins', 'radiomultikulti', 'funx', 'rtl radio', 'fm4', 'oe1', 'oe2 b', 'oe2 k', 'oe2 n', 'oe2 o', 'oe2 s', 'oe2 st', 'oe2 t', 'oe2 v', 'oe2 w', 'dradio wissen', 'ffn digital', 'radio paloma', 'wrn deutsch', 'radio maria', 'radio neue hoffnung', 'radio maryja', 'radio horeb', 'rnw1', 'rnw2', 'rnw3', 'mdr klassik', 'rtl 1440', 'nl-radio 1', 'nl-radio 5', 'nl-radio 6', 'sky radio', 'radio538', 'radio10gold', 'radio veronica', 'classic fm', 'hope channel radio', 'jam fm', 'radio gloria', 'radio regenbogen', 'u1 tirol', 'vrt radio1', 'vrt radio2', 'radio classique', 'tsf jazz', 'nostalgie', 'jazz radio', 'nrj', 'fun radio', 'slam fm', ':->Andere Sender']
}
