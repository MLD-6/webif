const fs = require('fs')
const fsp = fs.promises

module.exports = {
	get active() {
		return this._active
	},
	set active(value) {
		if (this.$root.auth.check()) {
			(async () => {
				this._active = await fsp.writeFile('/etc/vdr/diseqc.conf', value ? configs[value] : '').then(() => value).catch(() => '')
				fsp.appendFile('/etc/vdr/setup.conf.add', `!DiSEqC = ${this._active ? 1 : 0}\n`)
			})()
		}
	},

	get configs() {
		return Object.keys(configs)
	},

	async getConfig() {
		return await fsp.readFile(`/etc/vdr/diseqc.conf`, 'utf8').catch(() => '')
	},
	async setConfig(name, config) {
		if (this.$root.auth.check() && name) {
			this._active = await fsp.writeFile('/etc/vdr/diseqc.conf', config).then(() => name).catch(() => '')
			fsp.appendFile('/etc/vdr/setup.conf.add', `!DiSEqC = ${this._active ? 1 : 0}\n`)
			return true
		}
	}
}

var configs = {
	'DisiCon-4 Single Cable Network': `
		S19.2E  99999 H 10560 t v
		S19.2E  12110 V 11080 t v
		S19.2E  99999 V 10720 t v
	`,
	'Full DiSEqC': `
		S19.2E  11700 V  9750  [E0 10 38 F0]
		S19.2E  99999 V 10600  [E0 10 38 F1]
		S19.2E  11700 H  9750  [E0 10 38 F2]
		S19.2E  99999 H 10600  [E0 10 38 F3]
		S13.0E  11700 V  9750  [E0 10 38 F4]
		S13.0E  99999 V 10600  [E0 10 38 F5]
		S13.0E  11700 H  9750  [E0 10 38 F6]
		S13.0E  99999 H 10600  [E0 10 38 F7]
	`,
	'Full DiSEqC sequence AB': `
		S19.2E  11700 V  9750  t v W15 [E0 10 38 F0] W15 A W15 t
		S19.2E  99999 V 10600  t v W15 [E0 10 38 F1] W15 A W15 T
		S19.2E  11700 H  9750  t V W15 [E0 10 38 F2] W15 A W15 t
		S19.2E  99999 H 10600  t V W15 [E0 10 38 F3] W15 A W15 T
		S13.0E  11700 V  9750  t v W15 [E0 10 38 F4] W15 B W15 t
		S13.0E  99999 V 10600  t v W15 [E0 10 38 F5] W15 B W15 T
		S13.0E  11700 H  9750  t V W15 [E0 10 38 F6] W15 B W15 t
		S13.0E  99999 H 10600  t V W15 [E0 10 38 F7] W15 B W15 T
	`,
	'Mini DiSEqC AB': `
		S19.2E  11700 V  9750  t v W15 A W15 t
		S19.2E  99999 V 10600  t v W15 A W15 T
		S19.2E  11700 H  9750  t V W15 A W15 t
		S19.2E  99999 H 10600  t V W15 A W15 T
		S13.0E  11700 V  9750  t v W15 B W15 t
		S13.0E  99999 V 10600  t v W15 B W15 T
		S13.0E  11700 H  9750  t V W15 B W15 t
		S13.0E  99999 H 10600  t V W15 B W15 T
	`,
	'Mini DiSEqC BA': `
		S19.2E  11700 V  9750  t v W15 B W15 t
		S19.2E  99999 V 10600  t v W15 B W15 T
		S19.2E  11700 H  9750  t V W15 B W15 t
		S19.2E  99999 H 10600  t V W15 B W15 T
		S13.0E  11700 V  9750  t v W15 A W15 t
		S13.0E  99999 V 10600  t v W15 A W15 T
		S13.0E  11700 H  9750  t V W15 A W15 t
		S13.0E  99999 H 10600  t V W15 A W15 T
	`,
	'SCR 13V (Unicable)': `
		S19.2E  11700 V  9750  t v W10 S0 [E0 10 5A 00 00] W10 v
		S19.2E  99999 V 10600  t v W10 S1 [E0 10 5A 00 00] W10 v
		S19.2E  11700 H  9750  t v W10 S2 [E0 10 5A 00 00] W10 v
		S19.2E  99999 H 10600  t v W10 S3 [E0 10 5A 00 00] W10 v
		S13.0E  11700 V  9750  t v W10 S4 [E0 10 5A 00 00] W10 v
		S13.0E  99999 V 10600  t v W10 S5 [E0 10 5A 00 00] W10 v
		S13.0E  11700 H  9750  t v W10 S6 [E0 10 5A 00 00] W10 v
		S13.0E  99999 H 10600  t v W10 S7 [E0 10 5A 00 00] W10 v
	`,
	'SCR 18V (Unicable)': `
		S19.2E  11700 V  9750  t V W10 S0 [E0 10 5A 00 00] W10 v
		S19.2E  99999 V 10600  t V W10 S1 [E0 10 5A 00 00] W10 v
		S19.2E  11700 H  9750  t V W10 S2 [E0 10 5A 00 00] W10 v
		S19.2E  99999 H 10600  t V W10 S3 [E0 10 5A 00 00] W10 v
		S13.0E  11700 V  9750  t V W10 S4 [E0 10 5A 00 00] W10 v
		S13.0E  99999 V 10600  t V W10 S5 [E0 10 5A 00 00] W10 v
		S13.0E  11700 H  9750  t V W10 S6 [E0 10 5A 00 00] W10 v
		S13.0E  99999 H 10600  t V W10 S7 [E0 10 5A 00 00] W10 v
	`,
	'SCR JESS (Unicable 2)': `
		S19.2E  11700 V  9750  t V W10 S0 [70 00 00 00] W10 v
		S19.2E  99999 V 10600  t V W10 S1 [70 00 00 00] W10 v
		S19.2E  11700 H  9750  t V W10 S2 [70 00 00 00] W10 v
		S19.2E  99999 H 10600  t V W10 S3 [70 00 00 00] W10 v
	`
}
configs = Object.fromEntries(Object.entries(configs).map(([k, v]) => [k, v.replace(/^\s+/gm, '')]))
