const exec = require('../exec')

module.exports = {
	get missing() {
		return (async () => {return (await exec('find /lib/firmware/ -name *.not_found').catch(() => {}))?.stdout?.split('\n').filter(_ => _)})()
	},
}
