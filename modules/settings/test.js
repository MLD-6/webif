const setter = require('../object').setter

module.exports = {
	get value() {
		return setter(this.$root, this._value, this.updateValue)
	},
	set value(value) {
		this._value = value
		console.log('value')
		this.updateValue()
	},
	updateValue() {
		console.log('updateValue', this._value)
	},

	get syncGetter() {
		console.log(1, this.$root.test, this.$socket?.emit)
		return "syncGetter"
	},

	get asyncGetter() {
		return (async () => {
			console.log(2, this.$root.test)
			return 'asyncGetter'
		})()
	},

	set syncSetter(value) {
		console.log(3, this.$root.test, this.$socket?.server.emit)
		this.value = value
	},

	set asyncSetter(value) {
		(async () => {
			console.log(4, this.$root.test)
			this.value = value
		})()
	},

	syncFunction() {
		console.log(5, this.$root.test)
		return "syncFunction"
	},

	async asyncFunction() {
		console.log(6, this.$root.test)
		return "asyncFunction"
	},
}
