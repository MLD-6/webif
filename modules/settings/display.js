const exec = require('../exec')
const fs = require('fs')
const fsp = fs.promises
fsp.exists = async path => !!(await fsp.stat(path).catch(e => false))
const setter = require('../object').setter
const png = require("pngjs").PNG
const bmp = require("../bmp")

module.exports = {
	get active() {
		return setter(this.$root, this._active, () => {this.update()})
	},
	set active(value) {
		if (this.$root.auth.check()) {
			this._active = value
			this.update()
		}
	},

	get detect() {
		return (async () => {
			let ids = (await exec('sed -n "s/PCI_ID=//p" /sys/bus/pci/devices/*/uevent')).stdout.split('\n').filter(_ => _)
			return Object.keys(pciIds).find(name => pciIds[name].find(pciId => ids.find(id => id.startsWith(pciId))))
		})()
	},

	get devices() {
		return (async () => {return (await exec('(xrandr -d :0; xrandr -d :0.1) 2>/dev/null | grep "^\\S\\+ connected" | cut -d " " -f 1')).stdout.split('\n').filter(_ => _)})()
	},

	get drivers() {
		return (async () => {
			return [
				...((await fsp.exists('/sys/module/i915/initstate')) ? ['intel', 'iHD'] : []),
				...((await fsp.exists('/sys/module/nvidia/initstate')) ? ['nvidia'] : []),
				...((await fsp.exists('/sys/module/vboxvideo/initstate')) ? ['vbox'] : [])
			]
		})()
	},

	get driver() {
		return (async () => {
			return this.forceDriver || (await this.detect) || ''
		})()
	},

	get forceDriver() {
		return this._forceDriver || ''
	},
	set forceDriver(value) {
		if (this.$root.auth.check()) {
			this._forceDriver = value
			this.$root.services?.restart('xserver-nodm')
			this.$root.services?.restart('vdr')
		}
	},

	get forceHdmiOrAv() {
		return (async () => {
			let config = await fsp.readFile(`/boot/config.txt`, 'utf8').catch(() => '')
			return config.match(/^hdmi_force_hotplug=1/m) ? true : (config.match(/^hdmi_ignore_hotplug=1/m) ? false : null)
		})()
	},
	set forceHdmiOrAv(value) {
		if (this.$root.auth.check()) {
			if (value === null) {
				// no force
				exec(`test -e /boot/config.txt && sed 's/^hdmi_force_hotplug=.*/#hdmi_force_hotplug=/; s/^hdmi_ignore_hotplug=.*/#hdmi_ignore_hotplug=/' -i /boot/config.txt`).catch(() => {})
			} else if (value) {
				// force HDMI
				exec(`test -e /boot/config.txt && sed 's/#\\?hdmi_force_hotplug=.*/hdmi_force_hotplug=1/; s/^hdmi_ignore_hotplug=.*/#hdmi_ignore_hotplug=/' -i /boot/config.txt`).catch(() => {})
			} else {
				// force AV
				exec(`test -e /boot/config.txt && sed 's/^hdmi_force_hotplug=.*/#hdmi_force_hotplug=/; s/#\\?hdmi_ignore_hotplug=.*/hdmi_ignore_hotplug=1/' -i /boot/config.txt`).catch(() => {})
			}
		}
	},

	async getEdid(screen) {
		return (await exec(`xrandr -d :0 --props | grep -A100 "^${screen} connected " | grep EDID -m 1 -A 16 | grep ^$'\\t\\t'"[0-9a-f]\\{32\\}" | cut -b 3- | tr -d $'\\n'`)).stdout
	},

	get resolutions() {
		return resolutions
	},

	get rpiModes() {
		return rpiModes
	},

	update() {
		if (this.$root.auth.check()) {
			this._active?.forEach(async device => {
				exec(`setmode -o '${device.screen}' -m '${device.resolution}' -r '${device.refreshrate}'`).catch(() => {})
				device.edid = device.storeEdid ? (device.edid || (await this.getEdid(device.screen))) : ''
				device.storeEdid = !!device.edid
			})
			updateRpi(this._active?.[0])
		}
	},

	async updateSplash() {
		let size = (await fsp.readFile('/sys/class/graphics/fb0/virtual_size', 'utf8').catch(() => '800,600')).trim().split(',')

		if (!await fsp.exists(`/usr/share/plymouth/themes/spinner/watermark-${size[0]}x${size[1]}.png`) && await fsp.exists(`/usr/share/plymouth/themes/spinner/watermark-800x600.png`)) {
			await scalePng('/usr/share/plymouth/themes/spinner/watermark-800x600.png', `/usr/share/plymouth/themes/spinner/watermark-${size[0]}x${size[1]}.png`, 0, 0, Math.min(size[0] / 800, size[1] / 600))
		}
		if (await fsp.exists(`/usr/share/plymouth/themes/spinner/watermark-${size[0]}x${size[1]}.png`)) {
			exec(`ln -fs watermark-${size[0]}x${size[1]}.png /usr/share/plymouth/themes/spinner/watermark.png`).catch(() => {})
		}

		if (!await fsp.exists(`/boot/efi/boot/splash-${size[0]}x${size[1]}.png`) && await fsp.exists('/boot/efi/boot/splash-800x600.png')) {
			await scalePng('/boot/efi/boot/splash-800x600.png', `/boot/efi/boot/splash-${size[0]}x${size[1]}.png`, size[0], size[1])
		}
		if (await fsp.exists(`/boot/efi/boot/splash-${size[0]}x${size[1]}.png`)) {
			exec(`sed 's/RESOLUTION.*/RESOLUTION ${size[0]} ${size[1]}/; s/background.*/background splash-${size[0]}x${size[1]}.png/' -i /boot/efi/boot/syslinux.cfg`).catch(() => {})
		}

		if (!await fsp.exists(`/boot/logo-${size[0]}x${size[1]}.bmp`) && await fsp.exists('/boot/logo-1920x1080.bmp')) {
			await scaleBmp('/boot/logo-1920x1080.bmp', `/boot/logo-${size[0]}x${size[1]}.bmp`, 0, 0, Math.max(size[0] / 1920, size[1] / 1080))
		}
		if (await fsp.exists(`/boot/logo-${size[0]}x${size[1]}.bmp`)) {
			exec(`cp /boot/logo-${size[0]}x${size[1]}.bmp /boot/logo.bmp`).catch(() => {})
		}
	},
}

async function updateRpi(device) {
	if (device?.resolution && await fsp.exists('/boot/config.txt')) {
		let mode = rpiModes.find(mode => mode.resolution == device.resolution && mode.refreshrate == device.refreshrate) || rpiModes.find(mode => mode.resolution == device.resolution)
		if (mode) {
			exec(`sed 's/#\\?hdmi_group=.*/hdmi_group=${mode.group}/; s/#\\?hdmi_mode=.*/hdmi_mode=${mode.mode}/' -i /boot/config.txt`).catch(() => {})
			exec(`tvservice -e '${mode.group} ${mode.mode} HDMI'`).catch(() => {})
		}
	} else {
		exec(`sed 's/^hdmi_group=/#hdmi_group=/; s/^hdmi_mode=/#hdmi_mode=/' -i /boot/config.txt`).catch(() => {})
	}
}

async function scalePng(src, dst, width, height, scale) {
	return new Promise((resolve, reject) => {
		let srcPng = new png()
		fs.createReadStream(src).pipe(srcPng).on("parsed", function () {
			scale ||= Math.min(width / srcPng.width, height / srcPng.height)
			width ||= srcPng.width * scale
			height ||= srcPng.height * scale
			let dstPng = new png({width, height, filterType: 0})
			scaleImage(srcPng, dstPng, scale)
			dstPng.pack().pipe(fs.createWriteStream(dst)).on("finish", resolve)
		})
	})
}

async function scaleBmp(src, dst, width, height, scale) {
	return new Promise((resolve, reject) => {
		fs.readFile(src, async (err, data) => {
			let srcBmp = bmp.decode(data)
			scale ||= Math.min(width / srcBmp.width, height / srcBmp.height)
			width ||= srcBmp.width * scale
			height ||= srcBmp.height * scale
			// the logo should have a dimension multiple of 8
			width = Math.ceil(width / 8) * 8
			height = Math.ceil(height / 8) * 8
			let dstBmp = {width, height, data: Buffer.alloc(width * height * 4)}
			await scaleImage(srcBmp, dstBmp, scale)
			fs.writeFile(dst, bmp.encode(dstBmp).data, resolve)
		})
	})
}

async function scaleImage(src, dst, scale) {
	let offset = {x: (dst.width - src.width * scale) / 2, y: (dst.height - src.height * scale) / 2}
	for (let y = 0; y < dst.height; y++) {
		for (let x = 0; x < dst.width; x++) {
			let srcIndex = 0
			let dstIndex = (dst.width * y + x) << 2
			if (x >= offset.x && x <= dst.width-offset.x && y >= offset.y && y <= dst.width-offset.y) {
				srcIndex = (src.width * Math.floor((y - offset.y) / scale) + Math.floor((x - offset.x) / scale)) << 2
			}
			dst.data[dstIndex] = src.data[srcIndex]
			dst.data[dstIndex + 1] = src.data[srcIndex + 1]
			dst.data[dstIndex + 2] = src.data[srcIndex + 2]
			dst.data[dstIndex + 3] = src.data[srcIndex + 3]
		}
	}
}

const resolutions = [
	{label: 'Ultra HD', value: '3840x2160'},
	{label: 'Full HD', value: '1920x1080'},
	{label: 'HD', value: '1280x720'},
	{label: 'SD', value: '720x576'},
	{label: '800 x 600', value: '800x600'},
	{label: '640 x 480', value: '640x480'}
]

const rpiModes = [
	{resolution: '3840x2160', refreshrate: 50, mode: 96, group: 1},
	{resolution: '3840x2160', refreshrate: 60, mode: 97, group: 1},
	{resolution: '1920x1080', refreshrate: 50, mode: 31, group: 1},
	{resolution: '1920x1080', refreshrate: 60, mode: 16, group: 1},
	{resolution: '1280x720', refreshrate: 50, mode: 19, group: 1},
	{resolution: '1280x720', refreshrate: 60, mode: 4, group: 1},
	{resolution: '720x576', refreshrate: 50, mode: 18, group: 1},
	{resolution: '800x600', refreshrate: 60, mode: 9, group: 2},
	{resolution: '640x480', refreshrate: 60, mode: 4, group: 2},
]

const pciIds = {
	'virtual': [
		// VMware, VirtualBox
		'15AD:0405',
		'15AD:0710'
	],
	'nvidia-legacy': [
		// GPUs die nvidia-legacy brauchen: Keppler, Fermi und älter
		'10DE:07E',
		'10DE:08',
		'10DE:0A2',
		'10DE:0A3',
		'10DE:0A6',
		'10DE:0A7',
		'10DE:0AD',
		'10DE:0E2',
		'10DE:0E3',
		'10DE:100',
		'10DE:101',
		'10DE:102',
		'10DE:103',
		'10DE:104',
		'10DE:105',
		'10DE:106',
		'10DE:107',
		'10DE:108',
		'10DE:109',
		'10DE:10A',
		'10DE:10B',
		'10DE:10C',
		'10DE:10D',
		'10DE:11',
		'10DE:12',
		'10DE:B',
		'10DE:C',
		'10DE:D',
		'10DE:FC',
		'10DE:FD',
		'10DE:FE',
		'10DE:FF',
	],
	'nvidia': [
		// Maxwell GPUs die mit nvidia-legacy oder nvidia funktioniert
		'10DE:13',
		'10DE:14',
		// Pascal GPUs und neuer die nvidia brauchen
		'10DE:15',
		'10DE:1B',
		'10DE:1C',
		'10DE:1D',
		'10DE:1E',
		'10DE:1F',
		'10DE:21'
	],
	'iHD': [
		// Intel iHD GPUs
		'8086:56',
		'8086:4905',
		'8086:46',
		'8086:4C',
		'8086:4E5',
		'8086:9B',
		'8086:71',
		'8086:9A',
		'8086:8A',
		'8086:9B',
		'8086:3E9',
		'8086:3EA',
		'8086:318',
		'8086:59',
		'8086:19'
	],
	'intel': [
		// x86'er System, guess intel GPU
		'8086:'
	]
}