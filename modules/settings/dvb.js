const exec = require('../exec')

module.exports = {
	get count() {
		return (async () => {
			return parseInt((await exec(`ls /dev/dvb/adapter?/dvr? 2>/dev/null | wc -l`).catch(_ => 0)).stdout.trim()) || 0
		})()
	},

	get devices() {
		return (async () => {return {
			count: await this.count,
			kernel: await this.hasKernelDevices,
			sundtek: await this.hasSundtekDevices
		}})()
	},

	get hasKernelDevices() {
		return (async () => {
			return (await exec(`lsmod | grep -q dvb.core`).catch(_ => false)) && true
		})()
	},

	get hasSundtekDevices() {
		return (async () => {
			return (await exec(`lsusb | grep -q 'eb1a:51be\\|eb1a:51b2\\|2659:1210\\|2659:1401\\|2659:1405\\|2659:1802'`).catch(_ => false)) && true
		})()
	},

	async waitForDevices(timeout = 15) {
		return new Promise(async (resolve) => {
			let time = Date.now()
			let hasDvb = (await this.hasKernelDevices) || (await this.hasSundtekDevices)
			let wait = async () => {
				let count = await this.count
				if (Date.now() < (time + timeout * 1000) && count < (this._lastCount || (hasDvb && 1))) {
					return setTimeout(wait, 1000)
				}
				this._lastCount = count
				resolve()
			}
			wait()
		})
	}
}
