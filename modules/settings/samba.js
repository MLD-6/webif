const exec = require('../exec')

module.exports = {
	set password(value) {
		if (this.$root.auth.check()) {
			if (value) {
				exec(`echo -e "${value}\n${value}" | smbpasswd -s -a ${this.$root.auth.username}`).catch(() => {})
			} else {
				exec(`smbpasswd -n`).catch(() => {})
			}
		}
	}
}
