var _name = ''

module.exports = {
	get active() {
		return (async () => {
			if ((await this.$root.apps.app)?.title == 'Mediathek HbbTV') {
				return Object.keys(mediatheks).find(key => mediatheks[key] === this.url)
			} else {
				return ''
			}
		})()
	},

	get all() {
		return Object.keys(mediatheks)
	},

	get browser() {
		return (async () => {
			let apps = await this.$root.packages.installed()
			return ['Chromium', 'Chrome'].find(browser => apps.find(app => app.name.includes(browser)))
		})()
	},

	async open(name) {
		_name = name
		this.$root.apps.start('Mediathek HbbTV')
	},

	async url(name) {
		name ||= _name
		_name = ''
		if (name == 'extension') {
			return 'https://chromewebstore.google.com/detail/redorbit-hbbtv-emulator/mmgfafehampkahlmoahbjcjcmgmkppab'
		} else if (name) {
			return mediatheks[name]
		}
		return 'http://localhost/mediathekhbbtv?hideNavi'
	}
}

const mediatheks = {
	'ARD': 'http://hbbtv.ardmediathek.de/',
	'ZDF': 'https://hbbtv.zdf.de/zdfm3/',
	'Anixe': 'http://hbbtv01p.anixe.net/pub/anixehd/ticker/index.html',
	'arte': 'http://www.arte.tv/redbutton/index.html',
	'Pro7': 'https://hbbtv.redbutton.de/extern/redorbit/hbbtv/apps/mediathek/v3/web/p7de/home/p7de',
	'RTL2':	'http://hbbtv.rtl2.de/red/',
	'münchen.tv': 'http://hbbtv.bmt-technik.de/dvb-s/sesps/muenchen.tv/index.html',
	'o.tv': 'http://hbbtv.bmt-technik.de/mediathek/otv/index.html#redbutton',
	'REGIO TV': 'http://hbbtv.bmt-technik.de/dvb-s/sesps/RegioTV/index.html',
	'Franken TV': 'http://hbbtv.bmt-technik.de/mediathek/TVtouring/index.html',
	'a.tv': 'http://hbbtv.bmt-technik.de/mediathek/a.tv/index.html#noredbutton',
	'Ingolstadt': 'http://hbbtv.bmt-technik.de/mediathek/intv/index.html#noredbutton',
	'TVP Polonia': 'http://hbbtest.v3.tvp.pl/hub/index_polonia.php',
}
