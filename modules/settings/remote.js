const exec = require('../exec')
const fs = require('fs')
const fsp = fs.promises
fsp.exists = async path => !!(await fsp.stat(path).catch(e => false))
var controller = null

module.exports = {
	async devices() {
		let devices = []
		;(await exec(`ir-keytable 2>&1 | sed -n "s/.*\\/sys\\/class\\/rc\\/\\(.*\\)\\/.*/\\1/p" | while read device; do ir-keytable -s $device 2>&1 | sed -n "s/.*Name: /$device\\t/p"; done`)).stdout.split('\n').filter(_ => _).forEach(device => {
			let d = device.split('\t')
			devices.push({device: d[0], name: d[1]})
		})
		;(await exec(`ls -d /sys/class/hidraw/hidraw* 2>/dev/null | while read dev; do sed -n "s#HID_NAME=\\(.*\\)#\${dev##*/}\\t\\1#p" $dev/device/uevent | grep -i irmp; done; true`)).stdout.split('\n').filter(_ => _).forEach(device => {
			let d = device.split('\t')
			devices.push({device: d[0], name: d[1]})
		})
		// devices.push({device: 'hidraw6', name: 'Raspberry Pi Pico IRMP HID-KBD-Device'}) // Raspberry Pi Pico with IRMP firmware
		;(await exec(`blkid | sed -n 's/\\(.*\\):.*LABEL="RPI-RP2".*/\\1/p'`)).stdout.split('\n').filter(_ => _).forEach(device => {
			devices.push({device: device, name: 'Raspberry Pi Pico'}) // in boot mode
		})
		if(await exec(`lsusb | grep -q 2e8a:000a`).catch(_ => false)) {
			devices.push({name: 'Raspberry Pi Pico'}) // no firmware and not in boot mode
		}
		if(await exec(`lsusb | grep -q 10c4:876c`).catch(_ => false)) {
			devices.push({name: 'YaUsbIR', installed: (await this.$root.packages.installed()).some(pkg => pkg.name.includes('yausbir'))})
		}
		if(await exec(`lsusb | grep -q 'eb1a:51be\\|eb1a:51b2\\|2659:1210\\|2659:1401\\|2659:1405\\|2659:1802'`).catch(_ => false)) {
			let installed = (await this.$root.packages.installed()).some(pkg => pkg.name=='sundtek')
			devices.push({name: 'Sundtek', installed, map: installed ? (await this.sundtekRcMap()) : -1})
		}
		if (!devices.find(device => device.name == 'Serial IR type home-brew') && (await this.comports).length) {
			devices.push({name: 'Serial IR type home-brew'})
		}
		return devices
	},

	async installIrmpFirmware(device) {
		if (device && this.$root.auth.check()) {
			let firmwares = {'2e8a:0003': 'irmp-rp2040-firmware.uf2'} //, '2e8a:0003': 'irmp-rp2050-firmware.uf2'}
			let firmware = firmwares[(await exec(`lsusb | sed 's/.* \\(....:....\\).*/\\1/'`).catch(_ => _)).stdout.split('\n').find(id => id in firmwares)]
			if(firmware) {
				return await exec(`mkdir -p /mnt/rpi-rp2 && mount ${device} /mnt/rpi-rp2 && cp /lib/firmware/${firmware} /mnt/rpi-rp2/${firmware}; umount /mnt/rpi-rp2 && rmdir /mnt/rpi-rp2 && sleep 5`).catch(() => {})
			}
		}
		return false
	},

	async installDriver(name) {
		if (this.$root.auth.check()) {
			return await this.$root.packages.install(name)
		}
	},

	async sundtekRcMap(map) {
		if (map !== undefined && this.$root.auth.check()) {
			await exec(`sed '/^ir_disabled=/d' -i /etc/sundtek.conf`).catch(() => {})
			if (map >= 0) {
				await exec(`mediaclient --updaterc=${map}`).catch(() => {})
			} else {
				await exec(`echo 'ir_disabled=1' >> /etc/sundtek.conf`).catch(() => {})
			}
			await exec(`systemctl restart sundtek`).catch(() => {})
		}
		if(await exec(`grep -q '^ir_disabled=1' /etc/sundtek.conf`).catch(_ => false)) {
			return -1
		} else {
			map = parseInt((await exec('mediaclient --getrc | sed "s/.* //"').catch(_ => _)).stdout.trim())                                                                                                    
			return isNaN(map) ? -1 : map
		}
	},

	async gpiopin(pin) {
		if (pin && this.$root.auth.check()) {
			await exec(`sed "s/^dtoverlay=gpio-ir.*/dtoverlay=gpio-ir,gpio_pin=${pin}/g" -i /boot/config.txt`).catch(() => {})
		}
		return parseInt((await exec(`sed -n "s/^dtoverlay=gpio-ir,gpio_pin=\\([0-9]\\+\\).*/\\1/p" /boot/config.txt`).catch(_ => _)).stdout.trim()) || 18
	},

	get comports() {
		return (async () => {return (await exec(`dmesg | sed -n 's/.*ttyS\\([0-9]\\).*/\\1/p'`)).stdout.trim().split('\n').map(n => Number(n)+1)})()
	},

	async comport(port) {
		if (port !== undefined && this.$root.auth.check()) {
			this._comport = port
			await this.setSerial()
		}
		return this._comport
	},

	async setSerial() {
		if (this.$root.auth.check()) {
			await exec(`modprobe -r serial_ir`).catch(() => {})
			if (this._comport > 0) {
				if (!(await fsp.exists('/usr/bin/setserial'))) {
					await this.$root.packages.install('setserial')
				}
				await exec(`setserial /dev/ttyS${this._comport - 1} uart none`).catch(() => {})
				await exec(`modprobe serial_ir`).catch(() => {})
			}
		}
	},

	async protocol(device) {
		let protocol = ''
		let name = (await exec(`ir-keytable -s ${device} 2>&1 | sed -n "s/.*Name: \\(.*\\)/\\1/p"`)).stdout.trim().replace(/\s/g, '_').replace(/\W/g, '')
		if (await fsp.exists(`/etc/rc_keymaps/${name}.toml`)) {
			protocol = (await fsp.readFile(`/etc/rc_keymaps/${name}.toml`, 'utf8').catch(() => '')).split('\n').filter(row => row.match(/^protocol/))?.[0].replace(/\S*\s*=\s*"(\S*)"/, '$1')
		}
		if (!protocol) {
			protocol = (await exec(`ir-keytable -s ${device} 2>&1 | sed -n "s/.*Enabled.*protocols: \\(.*\\)/\\1/p"`)).stdout.split(/\s/).filter(_ => _)
			protocol = (protocol.length > 1 ? protocol.filter(protocol => protocol && protocol != 'lirc') : protocol)?.[0]
		}
		return protocol
	},

	async protocols(device) {
		return (await exec(`ir-keytable -s ${device} 2>&1 | sed -n "s/.*Supported.*protocols: \\(.*\\)/\\1/p"`)).stdout.split(/\s/).filter(_ => _)
	},

	get keys() {
		return Object.entries(keys).map(([name, key]) => ({name, key}))
	},

	async getKeymap(device) {
		if (device?.startsWith('hidraw')) {
			let map = []
			for (let i=0; !map.length && i<5; i++) {
				map = (await exec(`stm32kbdIRconfig_cmd /dev/${device} get keymap`).catch(_ => _)).stdout.trim().split('\n').filter(_ => _)
			}
			return map.map(row => row.split(' ')).filter(([code, key]) => code && key && code != 'ffffffffffff' && key != 'ff').map(([code, key]) => ({code, key}))
		} else if (device) {
			// return (await exec(`ir-keytable -s ${device} -r 2>/dev/null | grep -v KEY_RESERVED | sed -n 's/\\S\\+ \\(\\S\\+\\) = \\(KEY_\\S\\+\\) .*/\\1 = "\\2"/p'`)).stdout.split(/\n/).filter(_ => _)
			let name = (await exec(`ir-keytable -s ${device} 2>&1 | sed -n "s/.*Name: \\(.*\\)/\\1/p"`)).stdout.trim().replace(/\s/g, '_').replace(/\W/g, '')
			return (await fsp.readFile(`/etc/rc_keymaps/${name}.toml`, 'utf8').catch(() => '')).replace(/[\s\S]*\]/m, '').split('\n').filter(_ => _).map(row => {let m = row.match(/(\S*)\s*=\s*"(\S*)"/); return {code: m[1], key: m[2]}})
		}
	},
	
	async setKeymap(device, protocol, keymap) {
		keymap = keymap.filter(row => row.code)
		let status = false
		if (this.$root.auth.check()) {
			if (device.startsWith('hidraw')) {
				status = (await new Promise(async (resolve) => {
					let power = keymap.find(map => map.key == 'KEY_F12')
					if (power?.code) {
						await exec(`stm32kbdIRconfig_cmd /dev/${device} set wakeup ${power.code}`).catch(_ => _)
					}
					let command = exec.spawn('stm32kbdIRconfig_cmd', [`/dev/${device}`, 'set', 'keymap'])
					command.stdin.write(keymap.map(row => `${row.code} ${row.key}`).join('\n'))
					command.stdin.end()
					command.on('error', error => {console.error(error)})
					command.on('close', code => {resolve(!code)})
				}))
			} else if (device && protocol) {
				let name = (await exec(`ir-keytable -s ${device} 2>&1 | sed -n "s/.*Name: \\(.*\\)/\\1/p"`)).stdout.trim().replace(/\s/g, '_').replace(/\W/g, '')
				let driver = (await exec(`ir-keytable -s ${device} 2>&1 | sed -n "s/.*Driver: \\(.*\\)/\\1/p"`)).stdout.trim()
				if (keymap.length) {
					let table = (await exec(`ir-keytable -s ${device} 2>&1 | sed -n "s/.*Default keymap: \\(.*\\)/\\1/p"`)).stdout.trim()
					await fsp.writeFile(`/etc/rc_keymaps/${name}.toml`, `[[protocols]]\nname = "${name}"\nprotocol = "${protocol}"\n[protocols.scancodes]\n${keymap.map(row => `${row.code} = "${row.key}"`).join('\n')}\n`)
					await exec(`sed "/${name}\\.toml/d; /^#driver/a ${driver}\t${table}\t${name}.toml" -i /etc/rc_maps.cfg`)
					await exec(`ir-keytable -s ${device} -w /etc/rc_keymaps/${name}.toml`)
				} else {
					await exec(`sed "/${name}\\.toml/d" -i /etc/rc_maps.cfg`)
					await fsp.rm(`/etc/rc_keymaps/${name}.toml`).catch(() => {})
					await exec(`ir-keytable -s ${device} -c`)
				}
				status = true
			}
			await exec('udevadm trigger --subsystem-match=input --action=change')
		}
		return status
	},

	async getMacros(device) {
		if (device?.startsWith('hidraw')) {
			let macros = []
			for (let i=0; !macros.length && i<5; i++) {
				macros = (await exec(`stm32kbdIRconfig_cmd /dev/${device} get macros`).catch(_ => _)).stdout.trim().split('\n').filter(_ => _)
			}
			return macros.map(row => {
				let codes = row.split(' ')
				return codes.slice(0, codes.findIndex(code => code == 'ffffffffffff' || code == '000000000000'))
			}).filter(row => row.length && row[0] != 'ffffffffffff' && row[0] != '000000000000')
		}
	},
	
	async setMacros(device, macros) {
		macros = macros.filter(macro => macro.length > 1)
		let status = false
		if (this.$root.auth.check()) {
			if (device.startsWith('hidraw')) {
				status = (await new Promise(async (resolve) => {
					let command = exec.spawn('stm32kbdIRconfig_cmd', [`/dev/${device}`, 'set', 'macros'])
					command.stdin.write(macros.map(macro => macro.join(' ')).join('\n'))
					command.stdin.end()
					command.on('error', error => {console.error(error)})
					command.on('close', code => {resolve(!code)})
				}))
			}
		}
		return status
	},

	async getRepeat(device) {
		if (device?.startsWith('hidraw')) {
			return {
				delay: parseInt((await exec(`stm32kbdIRconfig_cmd /dev/${device} get repeat delay`).catch(_ => _)).stdout),
				period: parseInt((await exec(`stm32kbdIRconfig_cmd /dev/${device} get repeat period`).catch(_ => _)).stdout),
				timeout: parseInt((await exec(`stm32kbdIRconfig_cmd /dev/${device} get repeat timeout`).catch(_ => _)).stdout)
			}
		} else if (device) {
			let d = (await exec(`LC_ALL=C ir-keytable -s ${device} 2>&1 | sed -n "s/.*delay: \\([0-9]*\\).*period: \\([0-9]*\\).*/\\1\t\\2/p"`).catch(_ => _)).stdout.trim().split('\t')
			return {
				delay: parseInt(d[0]),
				period: parseInt(d[1]),
				timeout: 0
			}
		}
	},

	async setRepeat(device, repeat) {
		if (device?.startsWith('hidraw')) {
			await exec(`stm32kbdIRconfig_cmd /dev/${device} set repeat delay ${repeat.delay}`).catch(_ => _)
			await exec(`stm32kbdIRconfig_cmd /dev/${device} set repeat period ${repeat.period}`).catch(_ => _)
			await exec(`stm32kbdIRconfig_cmd /dev/${device} set repeat timeout ${repeat.timeout}`).catch(_ => _)
		} else if (device) {
			this.repeat ||= {}
			if (repeat) {
				this.repeat[device] = repeat
			} else {
				repeat = this.repeat[device]
			}
			if (repeat) {
				await exec(`ir-keytable -s ${device} -D ${repeat.delay} -P ${repeat.period}`)
			}
		}
	},

	async readkey(device, protocol) {
		return new Promise(async (resolve, reject) => {
			let command
			await exec(`systemctl stop keyd`).catch(_ => _)
			if (device.startsWith('hidraw')) {
				// ToDo: disable device during read key
				controller && controller.abort()
				controller = new AbortController()
				command = exec.spawn('stdbuf', ['-o0', 'stm32kbdIRconfig_cmd', `/dev/${device}`, 'quary', 'ir'], {signal: controller.signal})
				command.stdout.on('data', keycode => {
					keycode = keycode.toString().trim()
					if (keycode.length == 12) {
						this.$socket.emit("keycode", keycode)
					}
				})
			} else {
				await exec(`ir-keytable -s ${device} -c`) // Disable key handling
				controller && controller.abort()
				controller = new AbortController()
				command = exec.spawn('stdbuf', ['-o0', 'ir-keytable', '-s', device, '-p', protocol, '-t'], {signal: controller.signal})
				command.stdout.on('data', line => {
					let match = line.toString().match(/.* scancode = (\S+)/)
					if (match) {
						this.$socket.emit("keycode", match[1])
					}
				})
			}
			command.stderr.on('data', line => {
				console.log(line.toString())
			})
			command.on('error', error => {
				if (error.code != 'ABORT_ERR') {
					console.log(error)
				}
			})
			command.on('close', code => {resolve(!code)})
		})
	},

	async cancelReadkey(device) {
		controller && controller.abort()
		if (device) {
			if (device.startsWith('hidraw')) {
				// ToDo: enable device
			} else {
				await exec(`ir-keytable -s ${device} -a /etc/rc_maps.cfg`)
				await exec(`systemctl start keyd`)
			}
		}
	},

	async sendkey(device, keycode) {
		if (device.startsWith('hidraw')) {
			await exec(`stm32kbdIRconfig_cmd /dev/${device} emit ${keycode}`).catch(_ => _)
		}
	}
}

const keys = {
	'Menu': 'KEY_HOME',
	'Ok': 'KEY_ENTER',
	'Back': 'KEY_BACKSPACE',
	'Up': 'KEY_UP',
	'Down': 'KEY_DOWN',
	'Left': 'KEY_LEFT',
	'Right': 'KEY_RIGHT',
	'0': 'KEY_0',
	'1': 'KEY_1',
	'2': 'KEY_2',
	'3': 'KEY_3',
	'4': 'KEY_4',
	'5': 'KEY_5',
	'6': 'KEY_6',
	'7': 'KEY_7',
	'8': 'KEY_8',
	'9': 'KEY_9',
	'Red': 'KEY_F1',
	'Green': 'KEY_F2',
	'Yellow': 'KEY_F3',
	'Blue': 'KEY_F4',
	'Mute': 'KEY_F9',
	'Volume-': 'KEY_F10',
	'Volume+': 'KEY_F11',
	'Power': 'KEY_F12',
	'Play': 'KEY_PLAY',
	'Pause': 'KEY_PAUSE',
	'Stop': 'KEY_STOP',
	'Record': 'KEY_RECORD',
	'FastRew': 'KEY_REWIND',
	'FastFwd': 'KEY_FASTFORWARD',
	'Prev': 'KEY_PREVIOUSSONG',
	'Next': 'KEY_NEXTSONG',
	'Channel-': 'KEY_PAGEDOWN',
	'Channel+': 'KEY_PAGEUP',
	'PrevChannel': 'KEY_F5',
	'Info': 'KEY_F6',
	'Subtitles': 'KEY_F7',
	'Audio': 'KEY_F8',
	'Commands': 'KEY_F13',
	'Setup': 'KEY_F20',
	'Channels': 'KEY_F21',
	'Timers': 'KEY_F22',
	'Recordings': 'KEY_F23',
	'Schedule': 'KEY_F24',
	'User1': 'KEY_PROG1',
	'User2': 'KEY_PROG2',
	'User3': 'KEY_PROG3',
	'User4': 'KEY_PROG4',
	'User5': 'KEY_F14',
	'User6': 'KEY_F15',
	'User7': 'KEY_F16',
	'User8': 'KEY_F17',
	'User9': 'KEY_F18',
	'User0': 'KEY_F19',
}

// show remote key mapping: ir-keytable -r
// show key events on xorg: xev
// show keyboard mapping on xorg: xmodmap -pke
// emit key event on xorg: xdotool
