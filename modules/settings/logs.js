const exec = require('../exec')
const fs = require('fs')
const fsp = fs.promises

module.exports = {
	get units() {
		return (async () => {
			let units = []
			for (unit of (await exec("journalctl --field _SYSTEMD_UNIT")).stdout.split('\n')) {
				units.push({
					unit: unit.replace(/@.*/, '@*'),
					name: unit.startsWith('libpod') ? (await exec(`podman inspect --format '{{.Name}}' ${unit.replace(/.*-(.*).scope/, '$1')}`).catch(_=>_)).stdout : unit.replace(/\.service|@.*/, '')
				})
			}
			units = units.filter(unit => unit.name).sort((a,b) => a.name.localeCompare(b.name))
			return Array.from(new Set(units.map(JSON.stringify))).map(JSON.parse) // unique
		})()
	},

	async journal(unit='', reverse=false, page=1, pageSize=1000) {
		let opts = unit ? `-u ${unit}` : ''
		if (reverse) {
			return (await exec(`journalctl -r -n ${page * pageSize} ${opts} | awk 'NR >= ${(page - 1) * pageSize} && NR <= ${page * pageSize}'`)).stdout.split('\n').filter(_ => _)
		} else {
			return (await exec(`journalctl ${opts} | head -n ${page * pageSize} | awk 'NR >= ${(page - 1) * pageSize} && NR <= ${page * pageSize}'`)).stdout.split('\n').filter(_ => _)
		}
	},

	get persistent() {
		return (async () => {
			return /^Storage=persistent/m.test(await fsp.readFile('/etc/systemd/journald.conf', 'utf8').catch(() => ''))
		})()
	},
	set persistent(persistent) {
		if (persistent) {
			exec("test -L /var/log/ && rm /var/log; mkdir -p /var/log/journal; sed 's/^#Storage=.*/Storage=persistent/' -i /etc/systemd/journald.conf").catch(() => {})
		} else {
			exec("rm -r /var/log; ln -s volatile/log /var/log; sed 's/^Storage=/#Storage=/' -i /etc/systemd/journald.conf").catch(() => {})
		}
	}
}
