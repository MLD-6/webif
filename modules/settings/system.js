const cpu = require('cpu-stats')
const dns = require('dns').promises
const exec = require('../exec')
const fetch = require('node-fetch')
const fs = require('fs')
const fsp = fs.promises
fsp.exists = async path => !!(await fsp.stat(path).catch(e => false))
const ini = new (require('conf-cfg-ini'))({lineEnding: '\n'})
const md5 = require('md5')
const os = require('os')

var load = [{cpu: 0}]

module.exports = {
	announce: true,

	async announcing(force) {
		if (this.announce || force) {
			fetch(`${this.$root.mldUrl}/active.php?id=${this.id}&version=${await this.version}`)
		}
	},

	get arch() {
		return os.arch()
	},

	get hardware() {
		const archs = {x64: 'x86', x32: 'x86', arm: 'arm', arm64: 'arm'}
		return (async () => {
			return {
				arch: archs[this.arch], // x86, arm
				dvb: await this.$root.dvb.devices, // dvb, sundtek
				graphic: await this.$root.display.drivers, // intel, nvidia
				mach: await this.mach, // x86, rpi1, rpi2,...
				wlan: true, 
			}
		})()
	},

	get host() {
		return (async () => {
			let host = []
			for (let ip of this.ip) {
				let name = await dns.reverse(ip).catch(() => {})
				if (name) {
					host.push(name)
				}
			}
			return host.flat()
		})()
	},

	get hostname() {
		return (async () => {return (await exec('hostnamectl | sed -n "s/.* hostname: \\([^.]*\\).*/\\1/p" | tail -n1')).stdout.trim()})()
	},
	set hostname(value) {
		(async () => {
			await exec(`hostnamectl set-hostname ${value}`).catch(() => {})
			this.$socket?.server.emit('system.hostname', value)
		})()
	},

	get id() {
		return md5(Object.values(this.$root.network.interfaces)[0]?.[0].mac || '')
	},

	async info() {
		this.load.then(_load => load = _load)
		return {
			arch: os.arch(),
			disks: await this.$root.disks.mounted,
			hostname: os.hostname(),
			id: this.id,
			uptime: os.uptime(),
			load: load,
			loadavg: os.loadavg(),
			network: this.$root.network.interfaces,
			time: Date.now(),
			totalmem: os.totalmem(),
			usedmem: await this.usedmem,
			version: await this.version,
		}
	},

	get ip() {
		return Object.entries(this.$root.network.interfaces).map(([name, value]) => value[0].address)
	},

	get keymap() {
		return (async () => {return (await exec('localectl | sed -n "s/.* Keymap: \\(.*\\)/\\1/p"')).stdout.trim()})()
	},
	set keymap(value) {
		(async () => {
			await exec(`localectl set-keymap ${value}`).catch(() => {})
			this.$socket?.server.emit('system.keymap', this.keymap)
		})()
	},

	get keymaps() {
		return (async () => {return (await exec('localectl list-keymaps').catch(() => {}))?.stdout?.split('\n').filter(keymap => keymap) || [await this.keymap]})()
	},

	get locale() {
		return (async () => {return (await exec('localectl | sed -n "s/.* LANG=\\(.*\\)/\\1/p"')).stdout.trim()})()
	},
	set locale(value) {
		(async () => {
			if (value && value != 'C') {
				// install glibc package to selected locale
				await exec(`apt-get install glibc-binary-localedata-${value.replace('_', '-').toLowerCase()}`).catch(() => {})
			}
			await exec(`localectl set-locale ${value}`).catch(() => {})
			if (value && value != 'C') {
				// install packages to selected locale for all installed locale packages
				exec(`apt-get install -q -y \$( (dpkg-query --show -f="\\\${db:Status-Abbrev}\\\${Package}\\n" | sed -n "s/ii \\(.*\\)/\\1-locale-${value.replace(/_.*/, '')}\\n\\1-locale-${value.replace('_', '-').toLowerCase()}/p"; apt-cache --generate pkgnames) | sort | uniq -d)`).catch(() => {})
			}
			this.$socket?.server.emit('system.locale', this.locale)
		})()
	},

	get locales() {
		return (async () => {return (await exec('localectl list-locales | cut -d. -f1').catch(e => {console.log(e.message)}))?.stdout?.split('\n').filter(lc => lc) || [await this.locale]})()
	},

	get load() {
		return new Promise((resolve, reject) => {
			cpu(1000, (err, load) => {
				if (err) {
					return reject(err)
				}
				resolve(load)
			})
		})
	},

	get loadavg() {
		return os.loadavg()
	},

	get mach() {
		return (async () => {
			let cpu = await fsp.readFile('/proc/cpuinfo', 'utf8')
			if (cpu.includes('hypervisor')) return 'virtual'
			let model = (await fsp.readFile('/sys/firmware/devicetree/base/model', 'utf8').catch(() => ''))
			if (model.includes('Pi 1')) return 'rpi1'
			if (model.includes('Pi 2')) return 'rpi2'
			if (model.includes('Pi 3')) return 'rpi3'
			if (model.includes('Pi 4')) return 'rpi4'
			if (model.includes('ODROID')) return 'odroid'
			let product = await this.product // this is verry slow
			if (product.includes('Banana Pi')) return 'bpi'
			if (product.includes('Rock PI')) return 'rockpi'
			if (product.includes('Tinker')) return 'tinker'
			if (product.includes('Wetek')) return 'wetek'
			return 'x86'
		})()
	},

	async reboot() {
		return await exec.cmd('reboot', this.$socket)
	},

	async shutdown() {
		return await exec.cmd('shutdown -h now', this.$socket)
	},

	get product() {
		return (async () => {return (await exec('lshw -c system -quiet | sed -n "s/^    product: //p"')).stdout.trim()})()
	},

	get time() {
		return (async () => {
			let time = ini.decode((await exec('timedatectl show').catch(() => {}))?.stdout?.trim() || '')
			return {
				time: Date.now(),
				timezone: time.Timezone || Intl.DateTimeFormat().resolvedOptions().timeZone,
				ntp: time.NTP == 'yes',
				ntpsynced: time.NTPSynchronized == 'yes'
			}
		})()
	},

	get timezone() {
		return (async () => {return (await this.time).timezone})()
	},
	set timezone(value) {
		(async () => {
			await exec(`timedatectl set-timezone ${value}`).catch(() => {})
			this.$socket?.server.emit('system.timezone', await this.timezone)
		})()
	},

	get timezones() {
		return (async () => {return (await exec('timedatectl list-timezones').catch(() => {}))?.stdout.split('\n').filter(_ => _) || [await this.timezone]})()
	},

	get totalmem() {
		return os.totalmem()
	},

	get url() {
		return (async () => {
			let host = await this.host
			if (host[0]) {
				return `http://${host[0]}`
			} else {
				let ip = this.ip
				if (ip[0]) {
					return `http://${ip[0]}`
				} else {
					return ''
				}
			}
		})()
	},

	get usedmem() {
		return (async () => {return os.totalmem() - parseInt((await exec('free -k | sed -n "s/^Mem.* //p"')).stdout) * 1024})()
	},

	get version() {
		return (async () => {return (await exec('. /etc/os-release; echo $VERSION_ID')).stdout.trim()})()
	},
}
