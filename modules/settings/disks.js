const asyncScanLocal = require('../portscan').asyncScanLocal
const exec = require('../exec')
const fs = require('fs')
const fsp = fs.promises
const dir = require('node-dir')
const md5 = require('md5')

module.exports = {
	get mounted() {
		return (async () => {
			var devices = []
			for (let line of (await exec('df -Pk | sed "s/ \\+/\\t/g"').catch(() => {}))?.stdout.trim().split('\n').slice(1)) {
				let cl = line.split('\t')
				if (cl[0].startsWith('/')) {
					cl[0] = (await exec(`readlink -f '${cl[0]}'`).catch(() => {}))?.stdout.trim() || cl[0]
				}
				devices.push({
					source: cl[0],
					size: parseInt(cl[1], 10) * 1024,
					used: parseInt(cl[2], 10) * 1024,
					available: parseInt(cl[3], 10) * 1024,
					utilization: parseInt(cl[4], 10) / 100,
					mountpoint: cl[5]
				})
			}
			return devices.filter(device => device.source.startsWith('/dev/') && !device.source.startsWith('/dev/loop'))
		})()
	},

	get mountinfo() {
		return (async () => {
			let mounts = (await fsp.readFile('/proc/self/mountinfo', 'utf8').catch(() => '')).split('\n').filter(_ => _).map(line => {
				let values = line.split(' ')
				let subvol = (values[10]||values[9]).replace(/.*subvol=([^,]*).*/, '$1')
				return {source: {fs: values[8], device: values[9], path: values[3].replace(subvol, '') || '/'}, mountpoint: values[4]}
			})
			mounts.filter(mount => mount.source.device.match(/^[^/]+:\//)).forEach(mount => {
				let mnt = mounts.filter(mnt => mount.source.device.includes(mnt.source.device+'/')).reduce((a, b) => a && a.source.device.length <= b.source.device.length ? a : b, null)
				if (mnt) {
					mount.source.path = mount.source.device.substring(mnt.source.device.length) + mount.source.path
					mount.source.device = mnt.source.device
				}
			})
			if (mounts.find(mount => mount.source.fs == 'fuse.mergerfs')) {
				(await exec('ps -o args | grep ^mergerfs').catch(() => {}))?.stdout.split('\n').filter(_ => _).forEach(line => {
					let values = line.split(' ')
					mounts.filter(mount => mount.mountpoint == values[2]).forEach(mount => {
						mount.sources = values[1].split(':').map(source => {
							let dev = mounts.filter(mount => source.startsWith(mount.mountpoint)).sort((a, b) => b.mountpoint.length - a.mountpoint.length)[0]
							return {fs: mount.source.fs, device: dev.source.device, mountpoint: dev.mountpoint, path: source.replace(dev.mountpoint, '')}
						})
						delete mount.source
					})
				})
			}
			return mounts.filter(mount => mount.source?.device.startsWith('/dev/') || mount.source?.device.match(/^[^/]+:\//) || mount.sources?.find(source => source.device.startsWith('/dev/') || source.device.match(/^[^/]+:\//)))
		})()
	},

	get devices() {
		return (async () => {
			return (await exec('parted -lsm 2>/dev/null | sed -n "/^BYT/{n;p;}"').catch(() => {}))?.stdout.split('\n').filter(_ => _).map(line => {
				let values = line.split(':')
				return {name: values[6], device: values[0], size: values[1]}
			})
		})()
	},
	
	get partitions() {
		return (async () => {
			var device, devices = []
			;(await exec('parted -lsm 2>/dev/null').catch(() => {}))?.stdout.split('\n').forEach(line => {
				if (line.match(/^\/dev\//)) {
					device = line.split(':')
					devices.push({name: device[6], device: device[0], size: device[1], fs: device[2]})
				}
				if (line.match(/^\d/) && device[5] != 'loop') {
					let partition = line.split(':')
					devices.push({name: device[6], partition: partition[0], device: device[0]+(device[2].includes('mmc') || device[2].includes('nvme') ? 'p' : '')+partition[0], size: partition[3], fs: partition[4]})
				}
			})
			if ((await fsp.exists('/sbin/showmount'))) {
				let ips = await this.$root.system.ip
				for (let ip of (await asyncScanLocal(2049)).filter(ip => !ips.includes(ip))) {
					(await exec(`showmount -e ${ip} --no-headers | cut -d" " -f1`).catch(() => {}))?.stdout.split('\n').filter(_=>_).forEach(path => {
						devices.push({name: `NFS ${ip}${path.replaceAll('/', ' / ')}`, device: `${ip}:${path}`, size: '', fs: 'nfs'})
					})
				}
			}
			return devices
		})()
	},

	async pathes(device, path) {
		if (device) {
			let options = device.options || 'defaults'
			device = device.device || device
			let mountpoint = '/tmp/devices'
			await fsp.mkdir(mountpoint, {recursive: true})
			await exec(`mount ${device} ${mountpoint} -o ${options}`).catch(() => {})
			let pathes = await dir.promiseFiles(`${mountpoint}/${path}`, 'dir', {recursive: false}).catch(() => [])
			await exec(`umount ${mountpoint}`).catch(() => {})
			await fsp.rmdir(mountpoint).catch(() => {})
			return pathes?.map(path => path.replace(/.*\//, '')).sort() || []
		} else {
			let pathes = await dir.promiseFiles(path, 'dir', {recursive: false}).catch(() => [])
			return pathes?.map(path => path.replace(/.*\//, '')).sort() || []
		}
	},

	async hasFs(device) {
		let options = device.options || 'defaults'
		device = device.device || device
		let mountpoint = '/tmp/device_fs'
		await fsp.mkdir(mountpoint, {recursive: true})
		try {
			await exec(`mount ${device} ${mountpoint} -o ${options}`)
			await exec(`umount ${mountpoint}`).catch(() => {})
			await fsp.rmdir(mountpoint).catch(() => {})
			return true
		} catch(e) {
			return false
		}
	},

	async mkdir(device, path) {
		if (path && this.$root.auth.check()) {
			if (device) {
				let options = device.options || 'defaults'
				device = device.device || device
				let mountpoint = '/tmp/devices'
				await fsp.mkdir(mountpoint, {recursive: true})
				await exec(`mount ${device} ${mountpoint} -o ${options}`).catch(() => {})
				await fsp.mkdir(`${mountpoint}/${path}`, {recursive: true})
				await exec(`umount ${mountpoint}`).catch(() => {})
				await fsp.rmdir(mountpoint).catch(() => {})
			} else {
				await fsp.mkdir(path, {recursive: true})
			}
		}
	},

	async mkfs(device) {
		if (device && device.fs != 'nfs' && this.$root.auth.check()) {
			device = device.device || device
			await exec(`mkfs.xfs -q -f ${device}`)
		}
	},

	async mount(sources, mountpoint) {
		if (this.$root.auth.check()) {
			let fstab = (await fsp.readFile('/etc/fstab', 'utf8')).split('\n')
			for (source of sources) {
				if(source.device) {
					if (source.device.match(/^[^/]\S+:\//)) {
						source.options = source.options || 'soft,_netdev'
						source.mountpoint = `/mnt/${md5(source.device)}`
						if (!fstab.find(line => line.includes(` ${source.mountpoint} `))) {
							fstab.push(`${source.device} ${source.mountpoint} auto ${source.options} 0 0`)
						}
					} else {
						source.options = source.options || 'defaults'
						let uuid = (await exec(`blkid ${source.device} | sed 's/.*UUID="\\(\\S*\\)".*/\\1/'`).catch(() => {}))?.stdout.trim()
						source.mountpoint = `/mnt/${uuid}`
						if (!fstab.find(line => line.includes(` ${source.mountpoint} `))) {
							fstab.push(`UUID=${uuid} ${source.mountpoint} auto ${source.options} 0 2`)
						}
					}
					source.path = [source.mountpoint, source.path].filter(_=>_).join('/')
				}
			}
			if (mountpoint) {
				let netdev = sources.find(source => source.options.includes('_netdev'))
				if (sources.length == 1 && !netdev) {
					if (!fstab.find(line => line.includes(`${sources[0].path} ${mountpoint} `))) {
						fstab.push(`${sources[0].path} ${mountpoint} none bind 0 0`)
					}
				} else {
					let sourcePath = sources.map(source => source.path).join(':')
					let sourceDevice = sources.map(source => source.path.replace(source.mountpoint, source.device.replace(':', ''))).join(':')
					if (!fstab.find(line => line.includes(`${sourcePath} ${mountpoint} `))) {
						fstab.push(`${sourcePath} ${mountpoint} mergerfs category.create=epmfs,direct_io,use_ino,fsname=${sourceDevice}${netdev ? ',_netdev' : ''} 0 0`)
					}
				}
			}
			await fsp.writeFile('/etc/fstab', fstab.join('\n'))

			for (source of sources) {
				if(source.mountpoint) {
					await fsp.mkdir(source.mountpoint, {recursive: true})
					await exec(`mount ${source.mountpoint}`).catch(() => {})
				}
			}
			if (mountpoint) {
				await exec(`mount ${mountpoint}`)
			}
			return true
		}
	},

	async unmount(sources, mountpoint) {
		if (this.$root.auth.check()) {
			let fstab = (await fsp.readFile('/etc/fstab', 'utf8')).split('\n')
			if (mountpoint) {
				await exec(`umount ${mountpoint}`).catch(() => {})
				fstab = fstab.filter(line => !line.includes(` ${mountpoint} `))
			}
			for(source of sources) {
				if (source.device) {
					let mountpoint = '/mnt/' + ((await exec(`blkid ${source.device} | sed 's/.*UUID="\\(\\S*\\)".*/\\1/'`).catch(() => {}))?.stdout.trim() || md5(source.device))
					if (!fstab.find(line => line.split(' ')[0].includes(mountpoint))) {
						await exec(`umount ${mountpoint}`).catch(() => {})
						fstab = fstab.filter(line => !line.includes(` ${mountpoint} `))
					}
				}
			}
			await fsp.writeFile('/etc/fstab', fstab.join('\n'))
			return true
		}
	}
}
