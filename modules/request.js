const api = require('./api')
const express = require('express')
const request = express.Router()

request.head('/', async (req, res) => {
	res.sendStatus(200)
})

request.use(api.login)

request.get('/:name?', async (req, res) => {
	try {
		res.status(200).send((await api.get(req.params.name, {default: req.headers['default-value'], mode: req.headers.accept == 'application/sh' ? 'sh' : 'json'})).toString() + (req.headers['no-newline'] == 'true' ? '' : '\n'))
	} catch(error) {
		res.status(500).send(error.toString()+'\n')
	}
})

request.delete('/:name', async (req, res) => {
	try {
		if (await api.authCheck()) {
			res.status(200).send(await api.unset(req.params.name))
		} else {
			res.status(401).send('access denied\n')
		}
	} catch(error) {
		res.status(500).send(error.toString()+'\n')
	}
})

request.use((req, res, next) => {
	try {
		// read body as js
		req.body = ''
		req.setEncoding('utf8')
		req.on('data', chunk => { 
			req.body += chunk
		})
		req.on('end', () => {
			req.body = req.body.match(/^[\{\[\(\"\']|^\d+$|^true$|^false$/) ? eval(`(${req.body})`) : req.body
			next()
		})
	} catch (error) {
		res.status(500).send(error.toString()+'\n')
	}
})

request.post('/:name', async (req, res) => {
	try {
		res.status(200).send((await api.call(req.params.name, req.body, {default: req.headers['default-value'], mode: req.headers.accept == 'application/sh' ? 'sh' : 'json'})).toString() + (req.headers['no-newline'] == 'true' ? '' : '\n'))
	} catch(error) {
		res.status(500).send(error.toString()+'\n')
	}
})

request.put('/:name', async (req, res) => {
	try {
		if (await api.authCheck()) {
			res.status(200).send(await api.set(req.params.name, req.body))
		} else {
			res.status(401).send('access denied\n')
		}
	} catch(error) {
		res.status(500).send(error.toString()+'\n')
	}
})

module.exports = request
