module.exports = {setter, clone, deleteNull}

function setter (settings, obj, handler) {
	var proxy = {
		get: (target, key) => {
			return target[key] && typeof target[key] == 'object' ? new Proxy(target[key], proxy) : target[key]
		},
		set: (target, key, value) => {
			if (settings.auth.check()) {
				target[key] = value
				handler()
				return true
			}
		}
	}
	return obj && typeof obj == 'object' ? new Proxy(obj, proxy) : obj
}

function clone (obj) {
	var newObj = (obj instanceof Array) ? [] : {}
	for (var i in obj) {
		newObj[i] = obj[i] && typeof obj[i] == "object" ? clone(obj) : obj[i]
	}
	return newObj
}

function deleteNull(obj) {
	for (let key in obj) {
		if (obj[key] === null) {
			delete obj[key]
		} else if (typeof obj[key] == 'object') {
			deleteNull(obj[key])
		}
	}
	return obj
}
