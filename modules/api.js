const Settings = require('./settings')
const auth = require('basic-auth')
var request = null


const api = {
	authCheck: async () => {
		var settings = await Settings(request)
		return settings.auth.check()
	},

	login: async (req, res, next) => {
		request = req
		var settings = await Settings(request)
		settings.auth.login(auth(request)?.pass)
		next()
	},

	get: async (name = '', options = {}) => {
		try {
			var settings = await Settings(request)
			var value = settings
			var name_parts = name.split(' ')
			if (name) for (var prop of `.${name_parts.shift()}`.replace(/(\.|\[|\()/g, ':$1').split(':')) {
				if (!value && prop[0] == '(') {
					throw `Unknown function '${name.split(' ')[0]}'`
				}
				value = value && await eval(`value${prop}`)
			}
			if (name_parts.length) {
				value = eval(`value ${name_parts.join(' ')}`)
			}
			return format(options.mode, name, value, options.default)
		} catch (error) {
			console.error(error)
			return
		}
	},

	call: async (name, args = [], options = {}) => {
		var settings = await Settings(request)
		let value = await eval(`settings.${name}(...args)`)
		return format(options.mode, name, value, options.default)
	},

	set: async (name, value) => {
		var settings = await Settings(request)
		name.split('.').slice(0, -1).forEach(name => {
			if (!settings[name]) {
				settings[name] = {}
			}
			settings = settings[name]
		})
		settings[name.split('.').slice(-1)[0]] = value
	},

	unset: async (name) => {
		var settings = await Settings(request)
		eval(`delete settings.${name}`)
	}
}


function stringify(obj) {
	let type = Object.prototype.toString.call(obj)
	if (type == '[object Object]' || type == '[object Array]') {
		let values = []
		for (let key in obj) {
			let property = Object.getOwnPropertyDescriptor(obj, key)
			let value = ''
			if (property.get && property.set) {
				value = '[Getter/Setter]'
			} else if (property.get) {
				value = '[Getter]'
			} else if (property.set) {
				value = '[Setter]'
			} else {
				value = stringify(obj[key])
			}
			if (type == '[object Object]') {
				value = `${key}: ${value}`
			}
			values.push(value)
		}
		values = values.join(',\n').replace(/^/gm, '  ')
		return type == '[object Array]' ? `[\n${values}\n]` : `{\n${values}\n}`
	} else if (type == '[object Function]') {
		return '[Function]'
	} else if (type == '[object AsyncFunction]') {
		return '[AsyncFunction]'
	} else if (type == '[object String]') {
		return `'${obj}'`
	} else {
		return obj
	}
}

function format(mode, name, value, defaultValue) {
	switch (mode || 'json') {
		case 'json':
			if (typeof value == 'object' || typeof value == 'function') {
				return stringify(value)
			} else if (value !== undefined) {
				return value
			} else if (defaultValue !== undefined) {
				return defaultValue
			}
			return ''
		case 'sh':
			if (value instanceof Array) {
				var rows = []
				value.forEach((value, i) => {
					var values = []
					value = value instanceof Object ? value : {'value': value}
					Object.entries(value).filter(([key, value]) => !(value instanceof Object)).forEach(([key, value]) => {
						value = value === undefined ? (defaultValue === undefined ? '' : defaultValue) : (value instanceof String ? value.replace(/"/g, '\\"') : value)
						values.push(`${key}="${value}"`)
					})
					values.push(`i="${value.i === undefined ? i : value.i}"`)
					rows.push(values.join(';'))
				})
				return rows.join('\n')
			} else if (value instanceof Object) {
				var values = []
				Object.entries(value).filter(([key, value]) => !(value instanceof Object)).forEach(([key, value]) => {
					value = value === undefined ? (defaultValue === undefined ? '' : defaultValue) : (value instanceof String ? value.replace(/"/g, '\\"') : value)
					values.push(`${key}="${value}"`)
				})
				return values.join(';')
			} else {
				let key = name.split(' ')[0].split('.').at(-1).replace(/\[([^\]]*)\]/, '_$1')
				value = value === undefined ? defaultValue : (value instanceof String ? value.replace(/"/g, '\\"') : value)
				if (value !== undefined) {
					return `${key}="${value}"`
				}
			}
			return ''
	}
}

module.exports = api
