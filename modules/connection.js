const Settings = require('./settings')

module.exports = async (socket) => {
	var settings = await Settings(socket, () => {
		socket.emit('update', simplify(settings), handlers(settings))
	})
	socket.emit('start')

	socket.on('call', async (namespace, args, callback) => {
		var setting = settings
		namespace.forEach(name => {
			setting = setting && setting[name]
		})
		if (setting === undefined) {
			callback(undefined)
		} else {
			callback(await setting(...args).catch(console.error))
		}
	})

	socket.on('get', async (namespace, callback) => {
		if (namespace && !Array.isArray(namespace)) {
			namespace = namespace.split('.')
		}
		if (!namespace.length) {
			callback(simplify(settings))
		} else {
			let type = null
			var setting = settings
			namespace.forEach(name => {
				type = typeof Object.getOwnPropertyDescriptor(setting, name)?.value
				setting = setting && setting[name]
			})
			if (setting === undefined) {
				callback(undefined)
			} else if (type == 'object') {
				callback(simplify(setting))
			} else {
				callback(await setting)
			}
		}
	})

	socket.on('set', async (namespace, value, callback) => {
		if (settings.auth.check()) {
			var name = namespace.slice(-1)[0]
			var setting = settings
			namespace.slice(0, -1).forEach(name => {
				if (typeof setting[name] != 'object') setting[name] = {}
				setting = setting[name]
			})
			await (setting[name] = value)
			callback && callback(true)
		} else {
			callback && callback(false)
		}
	})

	socket.on('del', async (namespace, callback) => {
		if (settings.auth.check()) {
			var name = namespace.slice(-1)[0]
			var setting = settings
			namespace.slice(0, -1).forEach(name => {
				if (typeof setting[name] != 'object') setting[name] = {}
				setting = setting[name]
			})
			await (delete setting[name])
			callback && callback(true)
		} else {
			callback && callback(false)
		}
	})
}

function handlers(obj) {
	var to = Array.isArray(obj) ? [] : {}
	for (let key in obj) {
		let property = Object.getOwnPropertyDescriptor(obj, key)
		if (typeof property.value == 'object') {
			let value = handlers(obj[key])
			if (Object.keys(value).length) to[key] = value
		} else if (typeof property.value == 'function') {
			to[key] = "function"
		} else if (property.get && property.set) {
			to[key] = "getter/setter"
		} else if (property.get) {
			to[key] = "getter"
		} else if (property.set) {
			to[key] = "setter"
		}
	}
	return to
}

function simplify(obj) {
	var to = Array.isArray(obj) ? [] : {}
	for (let key in obj) {
		let property = Object.getOwnPropertyDescriptor(obj, key)
		if (typeof property.value == 'object' && property.value != null) {
			let value = simplify(obj[key])
			if (Object.keys(value).length != undefined) to[key] = value
		} else if (property.value !== undefined && typeof property.value != 'function') {
			to[key] = obj[key]
		}
	}
	return to
}
