'use strict';
const exec = require('./exec')

const df = async () => {
	var devices = []
	for (let line of (await exec('df -Pk | sed "s/ \\+/\\t/g"'))?.stdout.trim().split('\n').slice(1)) {
		let cl = line.split('\t')
		if (cl[0].startsWith('/')) {
			cl[0] = (await exec(`readlink -f '${cl[0]}'`))?.stdout.trim()
		}
		devices.push({
			source: cl[0],
			size: parseInt(cl[1], 10) * 1024,
			used: parseInt(cl[2], 10) * 1024,
			available: parseInt(cl[3], 10) * 1024,
			capacity: parseInt(cl[4], 10) / 100,
			mountpoint: cl[5]
		})
	}
	return devices
}

module.exports = df
module.exports.default = df
