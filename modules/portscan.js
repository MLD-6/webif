const os = require('os')
const Socket = require('net').Socket

module.exports = {asyncScanLocal, scanLocal, scan, check}


async function asyncScanLocal(port) {
	return new Promise(resolve => scanLocal(port, resolve))
}

function scanLocal(port, callback) {
	let found = []
	let running = 0
	const intervaces = os.networkInterfaces()
	for (const intervace in intervaces) {
		intervaces[intervace].forEach(iface => {
			if (iface.family == 'IPv4' && !iface.internal) {
				running++
				scan(port, iface.cidr, hosts => {
					found = found.concat(hosts)
					if (!--running) {
						callback(found)
					}
				})
			}
		})
	}
	if (!running) {
		callback(found)
	}
}

function scan(port, network, callback) {
	let hosts = []
	let running = 0
	for(var i=1; i<255; i++) {
		running++
		check(port, `${network.replace(/(\d+\.\d+\.\d+).*/, '$1')}.${i}`, (host, open) => {
			if (open) {
				hosts.push(host)
			}
			if (!--running) {
				callback(hosts)
			}
		})
	}
}

function check(port, host, callback) {
	let socket = new Socket()
	let open = false

	socket.setTimeout(1500)
	socket.on('connect', () => {open = true; socket.end()})
	socket.on('timeout', () => {socket.destroy()})
	socket.on('error', () => {})
	socket.on('close', () => {callback(host, open)})

	socket.connect(port, host);
}
