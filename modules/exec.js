const util = require('util')
const child_process = require('child_process')
const exec = util.promisify(child_process.exec)

exec.cmd = async (cmd, socket = null) => {
	var id = Date.now()
	try {
		const { stdout, stderr } = await exec(cmd)
		if (socket) {
			if (stdout || stderr) {
				socket.server.to(socket.conn.id).emit('log', id, `# ${cmd}\n`)
			}
			if (stdout) {
				socket.server.to(socket.conn.id).emit('log', id, stdout)
			}
			if (stderr) {
				socket.server.to(socket.conn.id).emit('log', id, stderr, true)
			}
		}
	} catch (e) {
		console.log(cmd, e.message)
		if (socket) {
			socket.server.to(socket.conn.id).emit('log', id, `${cmd} failed\n`, true)
			if (e.stderr) {
				socket.server.to(socket.conn.id).emit('log', id, e.stderr, true)
			}
		}
		return false
	}
	return true
}

exec.spawn = child_process.spawn

module.exports = exec
