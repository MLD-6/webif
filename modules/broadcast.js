module.exports = (socket, next) => {
	Object.defineProperty(socket.server, 'broadcast', {
		get() { 
			// broadcast to default namespace '/'
			return socket.server.sockets.sockets[socket.conn.id].broadcast
		},
	})

	next()
}
