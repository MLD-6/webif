const fs = require('fs')
const fsp = fs.promises
fsp.exists = async file => !!(await fsp.stat(file).catch(e => false))

module.exports = fsp