const dir = require('node-dir')
const fs = require('fs')
const fsp = fs.promises
const lock = require('lock').Lock()

const settingsdir = __filename.replace('.js', '/')
const settingsfile = __filename.replace(/.*\/(.*).js/, '/etc/$1.json')

const handlers = {
	debug: false,
	mldUrl: 'https://www.minidvblinux.de',
}
const store = {}
const listeners = []

// import all settings modules
dir.files(settingsdir, {sync:true}).filter(file => file.endsWith('.js')).sort().forEach(setting => {
	var path = setting.replace(settingsdir, '').replace('.js', '').split('/')
	var name = path.slice(-1)[0]
	var handler = handlers
	path.slice(0, -1).forEach(name => {
		handler[name] = handler[name] || {}
		handler = handler[name]
	})
	handler[name] = require(setting)
})

// merge settings
function merge(to, from) {
	for (let key in from) {
		let toProperty = Object.getOwnPropertyDescriptor(to, key)
		let fromProperty = Object.getOwnPropertyDescriptor(from, key)
		if (key.startsWith('_')) {
			Object.defineProperty(to, key, {enumerable: false, writable: true})
		}
		if (fromProperty.value !== null && typeof fromProperty.value == 'object' && (!toProperty || typeof toProperty.value == 'object')) {
			merge(to[key] ||= Array.isArray(from[key]) ? [] : {}, from[key])
		} else if (typeof fromProperty.value == 'function' || fromProperty.get || fromProperty.set) {
			Object.defineProperty(to, key, fromProperty)
		} else if (typeof toProperty?.value != 'object' && typeof toProperty?.value != 'function' && !toProperty?.get && !toProperty?.set) {
			to[key] = from[key]
		}
	}
	return to
}

function deleteNull(obj) {
	for (let key in obj) {
		if (obj[key] === null) {
			if (Array.isArray(obj)) {
				delete obj[key]
			}
		} else if (typeof obj[key] == 'object') {
			deleteNull(obj[key])
		}
	}
	return obj
}

// get settings values (without getters, setters or functions)
function simplify(obj) {
	var to = Array.isArray(obj) ? [] : {}
	for (let key of Object.getOwnPropertyNames(obj)) {
		if (!key.startsWith('__')) {
			let property = Object.getOwnPropertyDescriptor(obj, key)
			if (Object.prototype.toString.call(property.value) == '[object Object]') {
				let value = simplify(obj[key])
				if (Object.keys(value).length) to[key] = value
			} else if (property.value !== undefined && typeof property.value != 'function') {
				to[key] = obj[key]
			}
		}
	}
	return to
}

// sort object by key
function sortByKey(obj) {
	return obj && Object.prototype.toString.call(obj) == '[object Object]' ? Object.keys(obj).sort().reduce((c, d) => (c[d] = sortByKey(obj[d]), c), {}) : obj
}

// save settings
function save() {
	lock('save-settings', async (release) => {
		fsp.writeFile(settingsfile, JSON.stringify(sortByKey(simplify(store)), null, '\t')).catch(error => {console.error(error.toString())}).finally(release())
	})
}

// Load settings file
async function load() {
	var settings = {}
	try {
		settings = JSON.parse(await fsp.readFile(settingsfile, 'utf8'))
		if (Object.keys(store).length && JSON.stringify(settings) == JSON.stringify(sortByKey(simplify(store)))) {
			return
		}
	} catch(e) {
		console.error(`The settings file '${settingsfile}' is corrupt`)
		if (Object.keys(store).length) {
			return
		}
	}
	Object.keys(store).forEach(key => delete store[key])
	merge(merge(store, handlers), deleteNull(settings))
	
	listeners.forEach(listener => listener())
}

module.exports = async (connection, onchange) => {
	const proxy = {
		get: (target, key, receiver) => {
			let property = Object.getOwnPropertyDescriptor(target, key)
			let type = Object.prototype.toString.call(property?.value)
			if (type == '[object Object]' || type == '[object Array]') {
				return new Proxy(target[key], proxy)
			}
			if (typeof property?.value == 'function') {
				return target[key].bind(receiver)
			}
			if (key == '$root') {
				return root
			}
			if (key == '$socket') {
				return connection?.conn ? connection : undefined
			}
			if (key == '$request') {
				return connection?.headers ? connection : undefined
			}
			return Reflect.get(target, key, receiver)
		},
		set: (target, key, value, receiver) => {
			let property = Object.getOwnPropertyDescriptor(target, key)
			if (property?.set) {
				Reflect.set(target, key, value, receiver)
			} else {
				if (key.startsWith('_')) {
					Object.defineProperty(target, key, {enumerable: false, writable: true})
				}
				target[key] = value
				onchange && onchange()
				save()
			}
			return true
		},
		deleteProperty: (target, key) => {
			let property = Object.getOwnPropertyDescriptor(target, key)
			if (property?.get || property?.set) {
				target[key] = undefined
			} else {
				delete target[key]
			}
			onchange && onchange()
			save()
			return true
		},
	}
	var root = new Proxy(store, proxy)

	await load()

	if (onchange) {
		if (!listeners.length) {
			fs.watchFile(settingsfile, () => load())
		}
		listeners.push(onchange)
		setTimeout(onchange)
	}

	return root
}
