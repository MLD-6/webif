const exec = require('./exec')
const Settings = require('./settings')
const express = require('express')
const request = express.Router()
const bodyParser = require("body-parser")

var settings

request.use(function (req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*')
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST')
	res.setHeader('Access-Control-Allow-Headers', 'content-type')
	next();
})

request.use(bodyParser.json())

request.get('/', call)

request.post('/', call)

async function call(req, res) {
	try {
		settings ||= await Settings()
		let app = await settings.apps.app.title || 'VDR'
		let request = req.query.request || req.body
		if (methods[app]?.[request.method]) {
			let result = await methods[app][request.method](request)
			let response = {jsonrpc: request.jsonrpc, id: request.id, result}
			// console.log(request.method, request, response)
			res.status(200).send(response)
		} else {
			let result = `unknowd method '${request.method}'`
			// console.log(`jsonrpc: ${result}`)
			// // console.log(request.method, request)
			res.status(500).send(result+'\n')
		}
	} catch(error) {
		console.log(error)
		res.status(500).send(error.toString()+'\n')
	}
}

const methods = {
	VDR: {
		'JSONRPC.Ping'() {
			return 'OK'
		},

		// 'Playlist.GetPlaylists'() {
		// 	return [{playlistid: 1}]
		// },

		// async 'Playlist.GetItems'(request) {
		// 	if (request.params.properties.includes('file')) {
		// 		return (await settings.vdr.plugins.mpv.get()).map(file => ({file}))
		// 	}
		// 	return []
		// 	return {limits: {start: 0, end: 0, total: 0}}
		// },

		async 'Playlist.Clear'() {
			await settings.vdr.plugins.mpv.clear()
			return 'OK'
		},

		async 'Playlist.Add'(request) {
			let file = request.params.item.file
			if (file.includes('plugin://plugin.video.youtube')) {
				file = `https://www.youtube.com/watch?v=${file.replace(/.*id=/, '')}`
			}
			if (file.includes('plugin://plugin.video.sendtokodi')) {
				file = file.replace(/.*sendtokodi\/\?/, '')
			}
			await settings.vdr.plugins.mpv.add(file)
			return 'OK'
		},

		'Player.GetActivePlayers'() {
			return [{playerid: 1, type: 'video'}]
		},

		async 'Player.Open'(request) {
			if (request.params?.item?.file) {
				methods.VDR['Playlist.Add'](request)
			}
			await settings.vdr.plugins.mpv.play()
			return 'OK'
		},

		// async 'Player.GetProperties'(request) {
		// 	let result = {}
		// 	const properties = {
		// 		playlistid() {return 1},
		// 	}
		// 	for (let name of request.params.properties) {
		// 		if (properties[name]) {
		// 			result[name] = await properties[name]()
		// 		}
		// 	}
		// 	return result
		// },

		// async 'Player.GetItem'() {
		// 	return {item: {file: (await settings.vdr.plugins.mpv.get())[0] || ''}}
		// },

		// async 'Application.GetProperties'(request) {
		// 	let result = {}
		// 	const properties = {
		// 		version() {return '2.6.4'},
		// 		volume() {return 30},
		// 		muted() {return false},
		// 	}
		// 	for (let name of request.params.properties) {
		// 		if (properties[name]) {
		// 			result[name] = await properties[name]()
		// 		}
		// 	}
		// 	return result
		// },

		async 'Input.Up'() {
			await exec.cmd(`svdrpsend.sh HITK up`)
			return 'OK'
		},
		async 'Input.Down'() {
			await exec.cmd(`svdrpsend.sh HITK down`)
			return 'OK'
		},
		async 'Input.Left'() {
			await exec.cmd(`svdrpsend.sh HITK left`)
			return 'OK'
		},
		async 'Input.Right'() {
			await exec.cmd(`svdrpsend.sh HITK right`)
			return 'OK'
		},
		async 'Input.showOSD'() {
			await exec.cmd(`svdrpsend.sh HITK menu`)
			return 'OK'
		},
		async 'Input.ExecuteAction'() {
			await exec.cmd(`svdrpsend.sh HITK menu`)
			return 'OK'
		},
		async 'Input.Select'() {
			await exec.cmd(`svdrpsend.sh HITK ok`)
			return 'OK'
		},
		async 'Input.Back'() {
			await exec.cmd(`svdrpsend.sh HITK back`)
			return 'OK'
		},
	}
}

module.exports = request
