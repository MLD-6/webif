#!/usr/bin/env node
'use strict'

const api = require('../modules/api')

;(async () => {
	var wait = () => setTimeout(wait, 100); wait()
	var args = process.argv.slice(2)
	var action = args[0]
	args.splice(0, 1)
	var mode = 'json'
	if (args[0] == "--sh") {
		mode = 'sh'
		args.splice(0, 1)
	}
	var newline = '\n'
	if (args[0] == "-n") {
		newline = ''
		args.splice(0, 1)
	}
	var defaultvalue = undefined
	if (args[0] == "-d") {
		defaultvalue = args[1]
		args.splice(0, 2)
	}
	var name = args[0] || ''
	args.splice(0, 1)
	try {
		args = args.map(arg => arg.match(/^[0-9\{\[\(\"\']|^true$|^false$/) ? arg : `"${arg}"`).join()
		args = eval(`[${args}]`)

		switch (action) {
			case 'get':
				process.stdout.write((await api.get(name, {default: defaultvalue, mode})) + newline)
				break
			case 'call':
				process.stdout.write((await api.call(name, args, {default: defaultvalue, mode})) + newline)
				break
			case 'set':
				await api.set(name, args[0])
				break
			case 'unset':
				await api.unset(name)
				break
			case 'error': break
			default:
				console.error('Usage: setting get [--sh] [-n] [-d DEFAULT_VALUE] [NAME]')
				console.error('  or:  setting call [--sh] [-n] [-d DEFAULT_VALUE] NAME [ARGUMENTS...]')
				console.error('  or:  setting set NAME VALUE')
				console.error('  or:  setting unset NAME')
		}
	} catch (error) {
		console.error(error.toString())
	}
	wait = () => {}
})()