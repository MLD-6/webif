FROM debian:buster
MAINTAINER Claus Muus <mail@clausmuus.de>

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
 && apt-get dist-upgrade -y \
 && apt-get install -y \
    vim locales locales-all sudo openssh-server curl gnupg2 libcap2-bin git libpam0g-dev make g++

RUN echo "%sudo ALL=(ALL:ALL) NOPASSWD:ALL" >> /etc/sudoers \
 && echo "/etc/init.d/ssh start" >> /init.sh

# nodejs v14
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash \
 && apt-get update \
 && apt-get install -y \
    nodejs \
 && npm install -g @quasar/cli nodemon \
 && echo "cd /var/www; quasar dev &>>/var/log/node &" >> /init.sh \
 && echo "cd /var/www; nodemon -w connections -w modules -w app.js app.js &>>/var/log/node &" >> /init.sh

#COPY node /etc/init.d/node
#RUN chmod a+x /etc/init.d/node \
# && echo "/etc/init.d/node start" >> /init.sh \
# && setcap CAP_NET_BIND_SERVICE=+eip /usr/bin/node \
# && useradd node

RUN echo "tail -f /var/log/node" >> /init.sh
#RUN echo "sleep infinity" >> /init.sh

RUN chmod a+x /init.sh

VOLUME /var/www

EXPOSE 22
# GUI prod server and backend server
EXPOSE 80
# GUI dev server
EXPOSE 8080

CMD /init.sh
